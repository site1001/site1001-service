package com.site1001.repo.notification;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.site1001.domain.notification.Subscription;

@Component
public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {
	/** Find methods */
	
	public Page<Subscription> findAll(Pageable pageable);
	public Page<Subscription> findByTag(String tag, Pageable pageable);
	public List<Subscription> findByPushKey(String pushKey);
}
