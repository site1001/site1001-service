package com.site1001.repo.design;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.site1001.domain.design.LayerElement;

@Component
public interface LayerRepository extends JpaRepository<LayerElement, Long> {
	/** Find methods */
	
	public Page<LayerElement> findAllByDocNameAndPageIndex(String docName, int pageIndex, Pageable pageable);
	
	public List<LayerElement> findByDocNameAndPageIndexAndLayerType(String docName, int pageIndex, int layerType);
	
	public List<LayerElement> findByNameAndDocNameAndPageIndexAndLayerType(String name, String docuName, int pageIndex, int layerType);
}
