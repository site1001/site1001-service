package com.site1001.config;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.util.Assert;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import com.site1001.service.image.ImageProcessingMessageStore;

@Configuration
@PropertySource("classpath:application.properties")
@EnableMongoRepositories
public class MongoConfig extends AbstractMongoConfiguration {	
	private static final Logger logger = LoggerFactory.getLogger(MongoConfig.class);
	protected static final String MESSAGE_STORE_DBNAME = "messageStoreDb";
	
	@Value("${mongo.address}")
	private String mongoAddress;
	
	@Value("${mongo.dbname}")
	private String dbName;
	
	@Value("${mongo.username}")
	private String username;
	
	@Value("${mongo.password}")
	private String password;
	
	@Value("${image.msgstore.groupid}")
	private String imageMsgStoreGroupId;
	
	@Bean
	public GridFsTemplate gridFsTemplate() throws Exception {
		return new GridFsTemplate(mongoDbFactory(), mappingMongoConverter());
	}

	@Override
	protected String getDatabaseName() {
		return dbName;
	}
		
	private MongoClientURI getMongoClientUri() throws UnsupportedEncodingException {
		Assert.hasText(username, "Missing username.");
		Assert.hasText(password, "Missing password.");
		Assert.hasText(mongoAddress, "Missing address.");
		String uri = String.format("mongodb://%s:%s@%s", URLEncoder.encode(username, "UTF-8"), URLEncoder.encode(password, "UTF-8"), mongoAddress);
		return new MongoClientURI(uri);
	}
	
	@Override
	@Bean
	public Mongo mongo() {
		try {
			MongoClient client = new MongoClient(getMongoClientUri());
			return client;
		} catch (Exception ex) {
			// ERROR
			
			logger.error("An error occurred.", ex);
			return null;
		}
	}
	
	@Bean
	public MongoDatabase mongoDatabase() {
		try {
			@SuppressWarnings("resource")
			MongoClient client = new MongoClient(getMongoClientUri());
			final MongoDatabase db = client.getDatabase(getDatabaseName());
			return db;
		} catch (Exception ex) {
			// ERROR
			
			logger.error("An error occurred.", ex);
			return null;
		}
	}
	
	@Bean
	public GridFSBucket gridFsBucket() {
		try {
			@SuppressWarnings("resource")
			MongoClient client = new MongoClient(getMongoClientUri());
			MongoDatabase db = client.getDatabase(getDatabaseName());
			final GridFSBucket gfsb = GridFSBuckets.create(db);
			return gfsb;
		} catch (Exception ex) {
			// ERROR
			
			logger.error("An error occurred.", ex);
			return null;
		}
	}
		
	@Bean
	public ImageProcessingMessageStore imageProcessingMessageStore() {
		try {
			UUID groupId = UUID.fromString(imageMsgStoreGroupId);
			MongoDbFactory factory = new SimpleMongoDbFactory((MongoClient)mongo(), MESSAGE_STORE_DBNAME);
			final ImageProcessingMessageStore store = new ImageProcessingMessageStore(groupId, factory);
			return store;
		} catch (Exception ex) {
			// ERROR
			
			logger.error("An error occurred.", ex);
			return null;
		}
	}
}
