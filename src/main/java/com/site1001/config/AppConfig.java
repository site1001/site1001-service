package com.site1001.config;

import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.Executor;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.interfaces.ECPrivateKey;
import org.bouncycastle.jce.interfaces.ECPublicKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import nl.martijndwars.webpush.PushService;

import static nl.martijndwars.webpush.Utils.ALGORITHM;
import static nl.martijndwars.webpush.Utils.CURVE;
import static org.bouncycastle.jce.provider.BouncyCastleProvider.PROVIDER_NAME;

@Configuration
@PropertySource("classpath:application.properties")
@ComponentScan("com.site1001")
@EnableWebMvc
@EnableWebSecurity
@EnableScheduling
public class AppConfig extends WebMvcConfigurerAdapter implements WebApplicationInitializer {
	public static final String API_VERSION = "v1";
	
	private static final int DEFAULT_ASYNC_TIMEOUT = 60; // Seconds
	
	@Value("${push.publickey}")
	private String vapidPublicKey;
	
	@Value("${push.privatekey}")
	private String vapidPrivateKey;
	
	@Value("${push.subject}")
	private String pushSubject;
	
	@PostConstruct
	private void init() {
		// Set default time zone
		
	    TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	    
	    // Add Bouncy Castle security provider
	    
	    Security.addProvider(new BouncyCastleProvider());
	    
	    // Set Java 2D KCMS (Kodak Color Management System) - for performance
	    
	    System.setProperty("sun.java2d.cmm", "sun.java2d.cmm.kcms.KcmsServiceProvider");
	    System.setProperty("org.apache.pdfbox.rendering.UsePureJavaCMYKConversion", "true");
	}
		
    /** Date formatter for HTTP headers */
    
    @Bean(name = "httpDateFormat")
    public SimpleDateFormat httpDateFormat() {    	
    	final SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
	    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
	    return sdf;
    }
    
    @Bean(name = "threadPoolTaskExecutor")
    public Executor threadPoolTaskExecutor() {
        final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setThreadNamePrefix("tpExecutor-");
        executor.initialize();
        return executor;
    }
    
    @Bean(name = "imagePoolTaskExecutor")
    public Executor imagePoolTaskExecutor() {
        final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(8);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("imgExecutor-");
        executor.initialize();
        return executor;
    }
    
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
    public KeyPair generateKeyPair() throws InvalidAlgorithmParameterException, NoSuchProviderException, NoSuchAlgorithmException {
        ECNamedCurveParameterSpec parameterSpec = ECNamedCurveTable.getParameterSpec(CURVE);

        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(ALGORITHM, PROVIDER_NAME);
        keyPairGenerator.initialize(parameterSpec);

        return keyPairGenerator.generateKeyPair();
        
		/*
        KeyPair keyPair = generateKeyPair();

        byte[] publicKey = Utils.savePublicKey((ECPublicKey) keyPair.getPublic());
        byte[] privateKey = Utils.savePrivateKey((ECPrivateKey) keyPair.getPrivate());


        System.out.println("PublicKey:");
        System.out.println(BaseEncoding.base64Url().encode(publicKey));

        System.out.println("PrivateKey:");
        System.out.println(BaseEncoding.base64Url().encode(privateKey));
        */
    }
	
    @Bean
    public PushService pushService() {
    	try {
        	PushService pushService = new PushService();
        	//pushService.setSubject(pushSubject);
			//pushService.setPublicKey(vapidPublicKey);
	    	//pushService.setPrivateKey(vapidPrivateKey);
	    	return pushService;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
    	return null;
    }
    
    @Override
    public void configureAsyncSupport(AsyncSupportConfigurer configurer) {
        configurer.setDefaultTimeout(DEFAULT_ASYNC_TIMEOUT * 1000);
        super.configureAsyncSupport(configurer);
    }
    
	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		// Do nothing...
	}
}

