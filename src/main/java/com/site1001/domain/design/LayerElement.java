package com.site1001.domain.design;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonFormat;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class LayerElement implements Serializable {
	/*
	 * Serial version
	 */
	private static final long serialVersionUID = 3549734055051871079L;
	/*
	 * Identifier
	 */
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true)
	private Long id;
	/*
	 * Timestamps
	 */
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "UTC")
	private Date creationDate;
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "UTC")
	private Date modifiedDate;
	/*
	 * Active flag
	 */
	@Column(name = "active", nullable = false, length = 1, columnDefinition = "int default 1")
	private boolean active;
	/*
	 * Name, code & description
	 */
	@Column
	private String name;
	@Column
	private String code;
	@Column(name = "description", columnDefinition = "text", length = 4096, nullable = true)
	private String description;
	/*
	 * Document reference & type info
	 */
	@Column(name = "docName", columnDefinition = "text", length = 2084, nullable = true)
	private String docName;
	@Column
	private int pageIndex;
	@Column
	private int layerType;
	/*
	 * Polygon data
	 */
	@Column
    @ElementCollection(fetch = FetchType.EAGER, targetClass = Point.class)
	private List<Point> points;
	
	public LayerElement() {
		this.creationDate = this.modifiedDate = new Date();
		this.points = new ArrayList<Point>();
		this.active = true;
		this.pageIndex = 0;
	}
	
	public LayerElement(final List<Point> points) {
		this.points = points;
	}
	
	public Long getId() {
		return id;
	}
	
	public Date getCreationDate() {
		return creationDate;
	}
	
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	public Date getModifiedDate() {
		return modifiedDate;
	}
	
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public boolean getActive() {
		return active;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDocument(String docName) {
		this.docName = docName;
	}
	
	public String getDocument() {
		return docName;
	}
	
	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}
	
	public int getPageIndex() {
		return pageIndex;
	}
	
	public void setLayerType(int layerType) {
		this.layerType = layerType;
	}
	
	public int getLayerType() {
		return layerType;
	}
	
	public void setPoints(final List<Point> points) {
		this.points = points;
	}
	
	public List<Point> getPoints() {
		return points;
	}
	
	public void update(LayerElement other) {
		Assert.notNull(other, "Missing other entity.");
		creationDate = other.creationDate;
		modifiedDate = other.modifiedDate;
		active = other.active;
		name = other.name;
		code = other.code;
		description = other.description;
		docName = other.docName;
		pageIndex = other.pageIndex;
		layerType = other.layerType;
		points = other.points;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 17;
		result = prime * result + (creationDate != null ? creationDate.hashCode() : 0);
		result = prime * result + (modifiedDate != null ? modifiedDate.hashCode() : 0);
		result = prime * result + (active ? 17 : 31);
		result = prime * result + (name != null ? name.hashCode() : 0);
		result = prime * result + (code != null ? code.hashCode() : 0);
		result = prime * result + (description != null ? description.hashCode() : 0);
		result = prime * result + (docName != null ? docName.hashCode() : 0);
		result = prime * result + Integer.hashCode(layerType);
		result = prime * result + (points != null ? points.hashCode() : 0);
		result = prime * result + Integer.hashCode(pageIndex);
		return result;
	}
	 
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		LayerElement other = (LayerElement)obj;
		return (active == other.active)
			   && (layerType == other.layerType)
			   && (pageIndex == other.pageIndex)
			   && (docName == null ? other.docName == null : docName.equals(other.docName))
			   && (code == null ? other.code == null : code.equals(other.code))
			   && (name == null ? other.name == null : name.equals(other.name))
			   && (description == null ? other.description == null : description.equals(other.description))
			   && (creationDate == null ? other.creationDate == null : creationDate.equals(other.creationDate))
			   && (modifiedDate == null ? other.modifiedDate == null : modifiedDate.equals(other.modifiedDate))
			   && (points == null ? other.points == null : points.equals(other.points));
	 }
	 
	 @Override
	 public String toString() {
		 return String.format("LayerElement [id=%d, creationDate=%s, modifiedDate=%s, active=%s, name='%s', code='%s', description='%s', docName='%s', pageIndex=%d, layerType=%d, points=%s]",
				 			  id,
				 			  creationDate,
				 			  modifiedDate,
				 			  active,
				 			  name,
				 			  code,
				 			  description,
				 			  docName,
				 			  pageIndex,
				 			  layerType,
				 			  points.toString());
	 }
}
