package com.site1001.domain.design;

import java.io.Serializable;

public class Point implements Serializable {
	private static final long serialVersionUID = -5704441281255903147L;
	private double x; 
	private double y;
	
	public Point() {
		x = y = 0;
	}
	
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public void setX(double x) {
		this.x = x;
	}
	
	public double getX() {
		return x;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	public double getY() {
		return y;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 17;
		result = prime * result + Double.hashCode(x);
		result = prime * result + Double.hashCode(y);
		return result;
	}
	 
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		Point other = (Point)obj;
		return x == other.x && y == other.y;
	 }
	 
	 @Override
	 public String toString() {
		 return String.format("Point [x=%f, y=%f]", x, y);
	 }
}
