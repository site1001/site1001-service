package com.site1001.domain.notification;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

@Entity
public class Subscription implements Serializable {
	/*
	 * Serial id 
	 */
	private static final long serialVersionUID = 2926081915060917796L;
	/*
	 * Identifier
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	/*
	 * Data fields
	 */
	private String tag;
	@Column(columnDefinition = "TEXT", length = 2084)
	private String endpoint;
	@Column(columnDefinition = "TEXT", length = 128, unique = true, nullable = false)
	private String pushKey;
	@Column(columnDefinition = "TEXT", length = 256, unique = true, nullable = false)
	private String auth;
	
	public Subscription() {
	}
	
	public Subscription(final String tag, final String endpoint, final String pushKey, final String auth) {
		this.tag = tag;
		this.endpoint = endpoint;
		this.pushKey = pushKey;
		this.auth = auth;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setTag(String tag) {
		this.tag = tag;
	}
	
	public String getTag() {
		return tag;
	}
	
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	
	public String getEndpoint() {
		return endpoint;
	}
	
	@JsonSetter("key")
	public void setPushKey(String key) {
		this.pushKey = key;
	}
	
	@JsonGetter("key")
	public String getPushKey() {
		return pushKey;
	}
	
	public void setAuth(String auth) {
		this.auth = auth;
	}
	
	public String getAuth() {
		return auth;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 17;
		result = prime * result + (tag != null ? tag.hashCode() : 0);
		result = prime * result + (endpoint != null ? endpoint.hashCode() : 0);
		result = prime * result + (pushKey != null ? pushKey.hashCode() : 0);
		result = prime * result + (auth != null ? auth.hashCode() : 0);
		return result;
	}
	 
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		Subscription other = (Subscription)obj;
		return (tag == null ? other.tag == null : tag.equals(other.tag))
			   && (endpoint == null ? other.endpoint == null : endpoint.equals(other.endpoint))
			   && (pushKey == null ? other.pushKey == null : pushKey.equals(other.pushKey))
			   && (auth == null ? other.auth == null : auth.equals(other.auth));
	 }
	 
	 @Override
	 public String toString() {
		 return String.format("Subscription [id=%d, tag='%s', endpoint='%s', pushKey='%s', auth='%s']",
				 			  id,
				 			  tag,
				 			  endpoint,
				 			  pushKey,
				 			  auth);
	 }
}
