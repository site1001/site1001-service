package com.site1001.controller.file;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.resource.ResourceUrlProvider;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.site1001.config.AppConfig;
import com.site1001.controller.BaseController;
import com.site1001.service.file.FileInfo;
import com.site1001.service.file.FileService;

@RestController
@RequestMapping("/" + AppConfig.API_VERSION + "/file")
public class FileController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(FileController.class);
	
	@Autowired
	@Qualifier("httpDateFormat")
	private SimpleDateFormat dateFormat;
	
	@Autowired
	private FileService fileService;
	
	/** List */
	
	@Async
	@GetMapping(value = "/list")
	public CompletableFuture<ResponseEntity<List<FileInfo>>> 
				listFiles(@RequestParam(value = "contentType", required = false) String contentType,
					      @RequestParam(value = "page", required = false, defaultValue = "0") int page,
					      @RequestParam(value = "limit", required = false, defaultValue = "500") int limit) {
		return fileService
					.findAll(contentType, page, limit)
					.handle((result, ex) -> {
						if (ex != null) {
	    					// ERROR
							
							if (ex instanceof IllegalArgumentException) {
								// Bad argument
								
		    					logger.warn("Bad request.", ex);
			   			        return buildResponse(HttpStatus.BAD_REQUEST); 	
							} else {
								// Fatal
								
								logger.error("An error occurred.", ex);
								return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR); 
							}
						} else {
							// Return list...
							
							if (result.isEmpty()) {
								// No content
								
								logger.info(String.format("No results for contentType: %s.", contentType));
								return buildResponse(HttpStatus.NO_CONTENT);
							} else {
								// List of files
								
								logger.info(String.format("List of contentType: %s.", contentType));
								return ResponseEntity.ok(result);
							}
						}
					});
	}
	
	/** Find with metadata */
	
	@Async
	@GetMapping(value = "/findmetadata")
	public CompletableFuture<ResponseEntity<List<FileInfo>>> findByMetaData(HttpServletRequest request) {
		// Process parameters
		
		int page = 0, limit = 500;
		Map<String, String[]> parameters = request.getParameterMap();
		Map<String, String> metadata = new HashMap<String, String>();
		for (String key : parameters.keySet()) {
			String theValue = parameters.get(key)[0];
			if ("page".equals(key.toLowerCase())) {
				page = Integer.parseInt(theValue);
			} else if ("limit".equals(key.toLowerCase())) {
				limit = Integer.parseInt(theValue);
			} else {
				metadata.put(key, theValue);
			}
		}
		
		// Find with metadata
		
		return fileService
					.findByMetadata(metadata, page, limit)
					.handle((result, ex) -> {
						if (ex != null) {
	    					// ERROR
							
							if (ex instanceof IllegalArgumentException) {
								// Bad argument
								
		    					logger.warn("Bad request.", ex);
			   			        return buildResponse(HttpStatus.BAD_REQUEST); 	
							} else {
								// Fatal
								
								logger.error("An error occurred.", ex);
								return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR); 
							}
						} else {
							// Return list...
							
							if (result.isEmpty()) {
								// No content
								
								logger.info("No results found.");
								return buildResponse(HttpStatus.NO_CONTENT);
							} else {
								// List of files
								
								logger.info(String.format("Metadata results: %d.", result.size()));
								return ResponseEntity.ok(result);
							}
						}
					});
	}
	
	/** Download */
	
	@Async
	@GetMapping(value = "/download/**")
	public CompletableFuture<ResponseEntity<?>>
				downloadFile(@RequestParam(value = "legacy", required = false, defaultValue = "true") boolean legacy,
							 HttpServletRequest request,
							 HttpServletResponse response) {
		// Get the pathname
		
		String tmp = getPathnameFromRequest(request);
		
		// Do a legacy name check
		
		final String pathname;
		if (legacy && (tmp.indexOf("Files/") == 0)) {
			pathname = tmp.replace("Files/", "");
		} else {
			pathname = tmp;
		}
		
		// Get file
		
		return fileService
					.downloadFile(pathname, response)
					.handle((result, ex) -> {
						if (ex != null) {
							// ERROR
	    					
							if (ex instanceof NoSuchElementException) {
								// Not found
								
		    					logger.warn(String.format("File not found: %s.", pathname));
			   			        return buildResponse(HttpStatus.NOT_FOUND); 	
							} else if (ex instanceof IllegalArgumentException) {
								// Bad request
								
		    					logger.warn("Bad request.", ex);
			   			        return buildResponse(HttpStatus.BAD_REQUEST); 	
							} else {
								// Fatal
								
								logger.error("An error occurred.", ex);
								return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR); 
							}
						} else {
							// Sent...
							
							logger.info(String.format("Downloaded: %s.", pathname));
						    return buildResponse(HttpStatus.OK);
						}
					});
	}
	
	/** Upload */
	
	@Async
	@PostMapping(value = "/upload")
	public @ResponseBody CompletableFuture<ResponseEntity<List<String>>>
							uploadFiles(@RequestParam(value = "allowMultipleFiles", required = false, defaultValue = "false") boolean allowMultipleFiles,
										HttpServletRequest request) {
		// Check for multi-part upload
			
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if (!isMultipart) {
			// Invalid request
				
			String msg = "Not a multipart request.";
			logger.warn(msg);
			return CompletableFuture.completedFuture(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
		}

		// Upload files
		
		return fileService
					.uploadFiles(request, allowMultipleFiles)
					.handle((result, ex) -> {
						if (ex != null) {
							// ERROR
	    					
							if (ex instanceof IllegalStateException) {
								// Name already exists...
								
								logger.warn("Filename conflict.");
								return buildResponse(HttpStatus.CONFLICT);
							} else if (ex instanceof IllegalArgumentException) {
								// Bad request
								
		    					logger.warn("Bad request.", ex);
			   			        return buildResponse(HttpStatus.BAD_REQUEST); 	
							} else {
								// Fatal
								
								logger.error("An error occurred.", ex);
								return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR); 
							}
						} else {
							// Success...
							
							logger.info(String.format("Uploaded %d items.", result.size()));
						    return ResponseEntity.ok(result);
						}
					});
	}
	
	/** Replace */
	
	@Async
	@PostMapping(value = "/replace")
	public @ResponseBody CompletableFuture<ResponseEntity<List<String>>>
							replaceFiles(@RequestParam(value = "allowMultipleFiles", required = false, defaultValue = "false") boolean allowMultipleFiles,
										 HttpServletRequest request) {
		// Check for multi-part upload
			
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if (!isMultipart) {
			// Invalid request
				
			String msg = "Not a multipart request.";
			logger.warn(msg);
			return CompletableFuture.completedFuture(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
		}

		// Upload files
		
		return fileService
					.replaceFiles(request, allowMultipleFiles)
					.handle((result, ex) -> {
						if (ex != null) {
							// ERROR
	    					
							if (ex instanceof IllegalStateException) {
								// Name already exists...
								
								logger.warn("Filename conflict.");
								return buildResponse(HttpStatus.CONFLICT);
							} else if (ex instanceof IllegalArgumentException) {
								// Bad request
								
		    					logger.warn("Bad request.", ex);
			   			        return buildResponse(HttpStatus.BAD_REQUEST); 	
							} else {
								// Fatal
								
								logger.error("An error occurred.", ex);
								return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR); 
							}
						} else {
							// Success...
							
							logger.info(String.format("Uploaded %d items.", result.size()));
						    return ResponseEntity.ok(result);
						}
					});
	}
	
	/** Delete */
	
	@Async
	@DeleteMapping(value = "/delete/**")
	public CompletableFuture<ResponseEntity<FileInfo>> deleteFile(HttpServletRequest request) {
		final String pathname = getPathnameFromRequest(request);
		return fileService
					.delete(pathname)
					.handle((result, ex) -> {
						if (ex != null) {
	    					// ERROR
							
							if (ex instanceof NoSuchElementException) {
								// Not found
								
		    					logger.warn(String.format("File not found: %s.", pathname));
			   			        return buildResponse(HttpStatus.NOT_FOUND); 	
							} else if (ex instanceof IllegalArgumentException) {
								// Bad request
								
		    					logger.warn("Bad request.", ex);
			   			        return buildResponse(HttpStatus.BAD_REQUEST); 	
							} else {
								// Fatal
								
								logger.error("An error occurred.", ex);
								return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR); 
							}
						} else {
					    	// Delete...
							
					    	logger.info(String.format("Deleted: %s.", pathname));
					    	return ResponseEntity.ok().build();
						}
					});
	}
	
	/** Rename */
	
	@Async
	@PutMapping(value = "/rename")
	public CompletableFuture<ResponseEntity<Void>> 
					renameFile(@RequestParam(value = "from", required = true) String oldFilename,
							   @RequestParam(value = "to", required = true) String newFilename) {
		return fileService
					.rename(oldFilename, newFilename)
					.handle((result, ex) -> {
						if (ex != null) {
	    					// ERROR
	    					
							if (ex instanceof NoSuchElementException) {
								// Not found
								
		    					logger.warn(String.format("File not found: %s / %s.", oldFilename, newFilename));
			   			        return buildResponse(HttpStatus.NOT_FOUND); 	
							} else if (ex instanceof IllegalArgumentException) {
								// Bad request
								
		    					logger.warn("Bad request.", ex);
			   			        return buildResponse(HttpStatus.BAD_REQUEST); 	
							} else {
								// Fatal
								
								logger.error("An error occurred.", ex);
								return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR); 
							}
						} else {
					    	// Renamed...
							
					    	logger.info(String.format("Renamed: %s -> %s.", oldFilename, newFilename));
					    	return buildResponse(HttpStatus.OK);
						}
					});
	}
	
	/** File info */
	
	@Async
	@GetMapping(value = "/info/**")
	public CompletableFuture<ResponseEntity<String>> getFileInfo(HttpServletRequest request) {
		final String pathname = getPathnameFromRequest(request);
		return fileService
					.findFile(pathname)
					.handle((result, ex) ->  {
						if (ex != null) {
	    					// ERROR
	    					
							if (ex instanceof NoSuchElementException) {
								// Not found
								
		    					logger.warn("File not found.");
			   			        return buildResponse(HttpStatus.NOT_FOUND); 	
							} else if (ex instanceof IllegalArgumentException || ex instanceof IllegalStateException) {
								// Bad request
								
		    					logger.warn("Bad request.", ex);
			   			        return buildResponse(HttpStatus.BAD_REQUEST); 	
							} else {
								// Fatal
								
								logger.error("An error occurred.", ex);
								return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR); 
							}
						} else {
					    	// Return file info...
								
							try {
								// Convert to json
								
								logger.info(String.format("Info: %s.", pathname));
								ObjectMapper mapper = new ObjectMapper();
								String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(result);

								// Create response
								
							    final HttpHeaders httpHeaders = new HttpHeaders();
							    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
							    return new ResponseEntity<String>(json, httpHeaders, HttpStatus.OK);
							} catch (JsonProcessingException e) {
								// ERROR
								
								logger.error("An error occurred.", ex);
								return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR); 
							}
						}
					 });
	}
	
	/** Update metadata */
	
	@Async
	@PutMapping(value = "/metadata/**")
	public CompletableFuture<ResponseEntity<Void>> updateFileMetaData(HttpServletRequest request) {
		// Get metadata from parameters
		
		Map<String, String[]> parameters = request.getParameterMap();
		Map<String, String> metadata = new HashMap<String, String>();
		for (String key : parameters.keySet()) {
			metadata.put(key, parameters.get(key)[0]);
		}
		
		// Update and return
		
		final String pathname = getPathnameFromRequest(request);
		return fileService
					.updateMetaData(pathname, metadata)
					.handle((result, ex) -> {
						if (ex != null) {
	    					// ERROR
	    					
							if (ex instanceof NoSuchElementException) {
								// Not found
								
		    					logger.warn("File not found.");
			   			        return buildResponse(HttpStatus.NOT_FOUND); 	
							} else if (ex instanceof IllegalArgumentException || ex instanceof IllegalStateException) {
								// Bad request
								
		    					logger.warn("Bad request.", ex);
			   			        return buildResponse(HttpStatus.BAD_REQUEST); 	
							} else {
								// Fatal
								
								logger.error("An error occurred.", ex);
								return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR); 
							}
						} else {
					    	// Updated...
							
					    	logger.info(String.format("Updated metadata for: %s.", pathname));
					    	return buildResponse(HttpStatus.OK);
						}
					});
	}
	
	/** Helpers */
	
	private String getPathnameFromRequest(HttpServletRequest request) {
		Assert.notNull(request, "Missing request parameter!");
		ResourceUrlProvider urlProvider = (ResourceUrlProvider)request.getAttribute(ResourceUrlProvider.class.getCanonicalName());
		String pathname =
			urlProvider
				.getPathMatcher()
				.extractPathWithinPattern(
		            String.valueOf(request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE)),
		            String.valueOf(request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE)));
		return pathname;
	}
}
