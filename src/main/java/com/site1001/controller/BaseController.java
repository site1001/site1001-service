package com.site1001.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

public class BaseController {
	protected <T> ResponseEntity<T> buildResponse(HttpStatus httpStatus) {
		Assert.notNull(httpStatus, "Missing HTTP status.");
		return ResponseEntity.status(httpStatus).build();
	}
}
