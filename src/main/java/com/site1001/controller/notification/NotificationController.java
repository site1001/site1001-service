package com.site1001.controller.notification;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.site1001.controller.BaseController;
import com.site1001.domain.notification.Subscription;
import com.site1001.service.notification.NotificationService;

import java.util.concurrent.CompletableFuture;

import com.site1001.config.AppConfig;

@RestController
@RequestMapping("/" + AppConfig.API_VERSION + "/notify")
public class NotificationController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(NotificationController.class);
	
	@Autowired
	private NotificationService notificationService;
	
	/** Count */
	
	@Async
    @GetMapping(value = "/count")
    public CompletableFuture<ResponseEntity<Long>> count() {
		return notificationService
					.count()
					.handle((result, ex) -> {
						if (ex != null) {
							// ERROR
													
							logger.error("An error occurred.", ex);
							return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR);
						} else {
							// Return count
							
							logger.info(String.format("Found subscriptions: %d.", result.longValue()));
							return ResponseEntity.ok(result);
						}
					});
    }
	
	/** Get subscription */
	
	@Async
	@GetMapping("/{id}")
    public CompletableFuture<ResponseEntity<Subscription>> get(@PathVariable("id") Long id) { 
		return notificationService
					.get(id)
					.handle((result, ex) -> {
						if (ex != null) {
							// ERROR
													
							logger.error("An error occurred.", ex);
							return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR);
						} else {
							// Return count
							
							logger.info(String.format("Found subscription: %d.", id));
							return ResponseEntity.ok(result);
						}
					});
	}
	
    /** Create subscription */
    
	@Async
    @PostMapping(value = "/subscribe")
    public CompletableFuture<ResponseEntity<Void>> subscribe(@Validated @RequestBody Subscription subscription, UriComponentsBuilder ucBuilder) { 
    	return notificationService
	    			.save(subscription)
					.handle((result, ex) -> {
						if (ex != null) {
							// ERROR
							
							if (ex instanceof DuplicateKeyException) {
								// Already exists
								
								logger.warn("Already exists.");
								return buildResponse(HttpStatus.CONFLICT);
							} else if (ex instanceof IllegalArgumentException || ex instanceof IllegalStateException) {
								// Bad parameter
								
								logger.warn("Bad request.");
								return buildResponse(HttpStatus.BAD_REQUEST);
							} else {
								// Fatal
								
	    						logger.error("An error occurred.", ex);
	    						return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR);
							}
						} else {
							// Success
							
				    	    logger.info(String.format("Added subscription with key: %s.", subscription.getPushKey()));
				    		HttpHeaders headers = new HttpHeaders();
				    		headers.setLocation(ucBuilder.path("/" + AppConfig.API_VERSION + "/notify/{id}").buildAndExpand(subscription.getId()).toUri());
				    		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
						}
					});
    }
	
	/** Send notification */
	
	@Async
	@PostMapping(value = "/broadcast")
	public CompletableFuture<ResponseEntity<Long>> 
						broadcast(@RequestParam(value = "tag", required = true) String tag,
								  @RequestParam(value = "title", required = true) String title, 
								  @RequestParam(value = "message", required = true) String message) {
    	return notificationService
	    			.broadcast(tag, title, message)
					.handle((result, ex) -> {
						if (ex != null) {
							// ERROR
							
							if (ex instanceof IllegalArgumentException || ex instanceof IllegalStateException) {
								// Bad parameter
								
								logger.warn("Bad request.");
								return buildResponse(HttpStatus.BAD_REQUEST);
							} else {
								// Fatal
								
								logger.error("An error occurred.", ex);
								return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR);
							}
						} else {
							// Success
							
				    	    logger.info(String.format("Sent notification count: %d.", result));
							return buildResponse(HttpStatus.OK);
						}
					});
	}
}
