package com.site1001.controller.image;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.site1001.config.AppConfig;
import com.site1001.controller.BaseController;
import com.site1001.service.file.FileInfo;
import com.site1001.service.file.FileService;
import com.site1001.service.image.BoundingBox;
import com.site1001.service.image.ImageProcessingMessageStore;
import com.site1001.service.image.ImageService;
import com.site1001.service.image.PdfImageRequest;

@RestController
@RequestMapping("/" + AppConfig.API_VERSION + "/image")
public class ImageController extends BaseController {
	// Logger
	
	private static final Logger logger = LoggerFactory.getLogger(ImageController.class);
	
	// Constants
	
	public static final String DEFAULT_OUTPUT_FORMAT = "PNG";
	
	// Services
	
	@Autowired
	private ImageService imageService;
	
	@Autowired
	private FileService fileService;
	
	// Queues
	
	@Autowired
	private ImageProcessingMessageStore imageProcessingMessageStore;
	
	// Properties
	
	@Autowired
	@Qualifier("httpDateFormat")
	private SimpleDateFormat dateFormat;
	
	/** PDF info endpoints */
	
	@Async
	@GetMapping(value = "/pageCount")
	public CompletableFuture<ResponseEntity<Long>>
				getPDFInfoStream(@RequestParam(value = "filename", required = true) String filename)
					throws MalformedURLException, IOException, InterruptedException, ExecutionException {
		// Check filename
		
		FileInfo fileInfo = fileService.findFile(filename).get();
		if (fileInfo == null) {
			// Bad filename
			
			logger.warn("Cannot read PDF. Unknown filename.");
			return CompletableFuture.completedFuture(buildResponse(HttpStatus.BAD_REQUEST));
		}
		
		// Return
		
		InputStream inputStream = fileService.openFile(fileInfo).get();
		return imageService
					.getPDFInfo(inputStream)
					.handle((result, ex) -> {
	    				if (ex != null) {
	    					// ERROR
	    					
	    					logger.error("An error occurred.", ex);
	    			        return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR); 
	    				} else {
	    					// Info...
	    					
	    			    	logger.info(String.format("Got PDF info: %s, pages: %d", filename, result.getNumberOfPages()));
		    				return ResponseEntity.ok(Long.valueOf(result.getNumberOfPages()));
	    				}
					});
	}
	
	/** Image conversion endpoints */
	
	@Async("imagePoolTaskExecutor")
	@GetMapping(value = "/pdf2img")
	public CompletableFuture<ResponseEntity<Void>>
				convertPdfUrlToImage(@RequestParam(value = "filename", required = false, defaultValue = "") String filename,
									 @RequestParam(value = "legacy", required = false, defaultValue = "true") boolean legacy,
									 @RequestParam(value = "pageIndex", required = false, defaultValue = "0") int pageIndex,
									 @RequestParam(value = "dpi", required = false, defaultValue = "72") int dpi,
								     @RequestParam(value = "format", required = false, defaultValue = "PNG") String format) 
					throws MalformedURLException, IOException, InterruptedException, ExecutionException {
		// Check filename
		
		if (!StringUtils.hasText(filename)) {
    		// Missing
    		
    		logger.warn(filename + " is a bad filename.");
			return CompletableFuture.completedFuture(buildResponse(HttpStatus.BAD_REQUEST));
		}
		
		// Handle legacy filenames
		
		String source;
		if (legacy) {
			if (filename.indexOf("/Files/") == 0) {
				source = filename.replace("/Files/", "");
			} else if (filename.indexOf("Files/") == 0) {
				source = filename.replace("Files/", "");
			} else {
				source = filename;
			}
		} else {
			source = filename;
		}
		
		// Check if source file exists
		
		FileInfo fileInfo = fileService.findFile(source).get();
		if (fileInfo == null) {
			// Not found
			
			logger.warn(String.format("Not found: %s.", filename));
			return CompletableFuture.completedFuture(buildResponse(HttpStatus.NOT_FOUND));
		}
		
    	// Check output file format
    	
    	String fileFormat = format != null ? format.trim().toLowerCase() : null;
    	if (!StringUtils.hasText(fileFormat) || !("png".equals(fileFormat) || "jpg".equals(fileFormat))) {
    		// Invalid format
    		
    		logger.warn(format + " is not a valid format.");
			return CompletableFuture.completedFuture(buildResponse(HttpStatus.BAD_REQUEST));
    	}

    	// Create document processing request
    	
		final PdfImageRequest request = new PdfImageRequest(source, pageIndex, dpi, fileFormat);
    	
    	// Check if mapped image file already exists (is processed)

		final String mappedFilename = request.getMappedFilename();
		fileInfo = fileService.findFile(mappedFilename).get();
		if (fileInfo != null) {
			// Bad filename
			
			logger.info(String.format("Found image: '%s' -> '%s'.", source, mappedFilename));
			return CompletableFuture.completedFuture(buildResponse(HttpStatus.RESET_CONTENT)); // Trigger client redraw
		}
				
	    // Add request to the queue...
	    						
	    logger.info(String.format("Added document to queue: %s.", filename));
	    imageProcessingMessageStore.push(request);
	    return CompletableFuture.completedFuture(buildResponse(HttpStatus.ACCEPTED));
	}
	
	/*
	@Async("imagePoolTaskExecutor")
	@GetMapping(value = "/pdf2img/{filename:.+}/{pageInfo:.+}")
	public CompletableFuture<ResponseEntity<?>>
				convertPdfFileToImage(@PathVariable("filename") String filename,
									  @PathVariable("pageInfo") String pageInfo,
									  @RequestParam(value = "dpi", required = false, defaultValue = "72") int dpi,
									  @RequestParam(value = "format", required = false, defaultValue = "PNG") String format,
									  HttpServletResponse response) 
					throws IOException, InterruptedException, ExecutionException {
		// Check page info
		
		if (!StringUtils.hasText(pageInfo)) {
    		// Invalid format
    		
    		logger.warn("Invalid page info.");
			return CompletableFuture.completedFuture(buildResponse(HttpStatus.BAD_REQUEST));
		}
		
		// Parse page info
		
		int pageIndex, dotIndex = pageInfo.indexOf('.');
		if (dotIndex == -1) {
			pageIndex = Integer.parseInt(pageInfo);
		} else {
			String tmp = pageInfo.substring(0,  dotIndex);
			pageIndex = Integer.parseInt(tmp);
			format = pageInfo.substring(dotIndex + 1).toUpperCase();
		}
		
		// Convert PDF to image...
		
		FileInfo fileInfo = fileService.findFile(filename).get();
		return convertPdfToImage(response, fileService.openFile(fileInfo).get(), pageIndex, dpi, filename, format);
	}
	*/
	
	/*
	private CompletableFuture<ResponseEntity<?>> 
				convertPdfToImage(HttpServletResponse response, InputStream inputStream, int pageIndex, int dpi,  String source, String format)
					throws IOException {
    	// Check output file format
    	
    	String fileFormat = format != null ? format.trim().toLowerCase() : null;
    	if (!StringUtils.hasText(fileFormat) 
    		|| !("png".equals(fileFormat) || "jpg".equals(fileFormat))) {
    		// Invalid format
    		
    		logger.warn(format + " is not a valid format.");
			return CompletableFuture.completedFuture(buildResponse(HttpStatus.BAD_REQUEST));
    	}
    	    	
	    // Set content & header info
    		
	    Date now = new Date();
	    response.setContentType("image/" + ("jpg".equals(fileFormat) ? "jpeg" : fileFormat.toLowerCase()));
	    response.setHeader("Cache-Control", "public, max-age=3600");
	    response.setHeader("Last-Modified", dateFormat.format(now));
	    		
	    // Start processing...
	    	
    	OutputStream outputStream = response.getOutputStream();
	    return imageService
	    			.convertPdfToImage(outputStream, fileFormat, inputStream, pageIndex, dpi)
	    			.handle((result, ex) -> {
	    				if (ex != null) {
	    					// ERROR
	    					
	    					logger.error("An error occurred.", ex);
	    			        return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR); 
	    				} else {
	    					// Converted...
	    					
	    			    	logger.info(String.format("Converted PDF (%s) to format:%s, page:%d, dpi:%d.", source, fileFormat, pageIndex, dpi));
		    				return buildResponse(HttpStatus.OK);
	    				}
	    			});
	}
	*/
	
	/** Flip endpoints */
	
	@Async("imagePoolTaskExecutor")
	@GetMapping(value = "/flip/{direction}/{filename:.+}")
	public CompletableFuture<ResponseEntity<?>>
				flipFile(@PathVariable("filename") String filename, @PathVariable("direction") int direction, HttpServletResponse response) 
					throws IOException, InterruptedException, ExecutionException {
		// Check parameters
		
		if (direction < 0 || direction > 1) {
			// Bad flip direction
			
			logger.warn("Unknown flip direction specified.");
			return CompletableFuture.completedFuture(buildResponse(HttpStatus.BAD_REQUEST));
		}
		
		// Check filename
		
		FileInfo fileInfo = fileService.findFile(filename).get();
		if (fileInfo == null) {
			// Bad filename
			
			logger.warn("Cannot flip image. Unknown filename.");
			return CompletableFuture.completedFuture(buildResponse(HttpStatus.BAD_REQUEST));
		}
		
		// Resize
		
		logger.info(String.format("Flip (%d) filename (%s).", direction, filename)); 
		OutputStream outputStream = response.getOutputStream();
		InputStream inputStream = fileService.openFile(fileInfo).get();
		return doFlip(outputStream, inputStream, direction);
	}
	
	@Async("imagePoolTaskExecutor")
	@GetMapping(value = "/flip")
	public CompletableFuture<ResponseEntity<?>>
				flipStream(@RequestParam(value = "source", required = true) String source,
						   @RequestParam(value = "direction", required = true) int direction,
						   HttpServletResponse response) 
					 throws MalformedURLException, IOException, InterruptedException, ExecutionException {
		// Check parameters
		
		if (direction < 0 || direction > 1) {
			// Bad flip direction
			
			logger.warn("Unknown flip direction specified.");
			return CompletableFuture.completedFuture(buildResponse(HttpStatus.BAD_REQUEST));
		}
		
		// Resize
		
		logger.info(String.format("Flip (%d) URL (%s).", direction, source)); 
		OutputStream outputStream = response.getOutputStream();
		InputStream inputStream = new URL(source).openStream();
		return doFlip(outputStream, inputStream, direction);
	}
	
	private CompletableFuture<ResponseEntity<?>> doFlip(OutputStream outputStream, InputStream inputStream, int direction) {
		return imageService
					.flip(outputStream, DEFAULT_OUTPUT_FORMAT, inputStream, direction)
					.handle((result, ex) -> {
						if (ex != null) {
							// ERROR
							
							logger.error("Could not flip image.", ex);
							return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR);
						} else {
							// Image flipped
							
							return buildResponse(HttpStatus.OK);
						}
					});
	}
	
	/** Resize endpoints */
	
	@Async("imagePoolTaskExecutor")
	@GetMapping(value = "/resize/{width}x{height}/{filename:.+}")
	public CompletableFuture<ResponseEntity<?>>
				resizeFile(@PathVariable("width") int width,
						   @PathVariable("height") int height,
						   @PathVariable("filename") String filename,
						   HttpServletResponse response) 
					throws IOException, InterruptedException, ExecutionException {
		// Check parameters
		
		if (width < 0 || height < 0) {
			// Bad target size
			
			logger.warn("Bad dimensions specified.");
			return CompletableFuture.completedFuture(buildResponse(HttpStatus.BAD_REQUEST));
		}
		
		// Check filename
		
		FileInfo fileInfo = fileService.findFile(filename).get();
		if (fileInfo == null) {
			// Not found...
			
			logger.warn("Cannot resize image. Unknown filename.");
			return CompletableFuture.completedFuture(buildResponse(HttpStatus.BAD_REQUEST));
		}
		
		// Resize
		
		logger.info(String.format("Resizing filename (%s) -> %d x %d", filename, width, height)); 
		OutputStream outputStream = response.getOutputStream();
		InputStream inputStream = fileService.openFile(fileInfo).get();
		return doResize(outputStream, inputStream, width, height);
	}
	
	@Async("imagePoolTaskExecutor")
	@GetMapping(value = "/resize")
	public CompletableFuture<ResponseEntity<?>>
				resizeStream(@RequestParam(value = "source", required = true) String source,
							 @RequestParam(value = "width", required = true) int width,
							 @RequestParam(value = "height", required = true) int height,
							 HttpServletResponse response) 
					throws MalformedURLException, IOException, InterruptedException, ExecutionException {
		// Check parameters
		
		if (width < 0 || height < 0) {
			// Bad target size
			
			logger.warn("Bad dimensions specified.");
			return CompletableFuture.completedFuture(buildResponse(HttpStatus.BAD_REQUEST));
		} else if (!StringUtils.hasText(source)) {
			// Missing source
			
			logger.warn("Missing source for resize.");
			return CompletableFuture.completedFuture(buildResponse(HttpStatus.BAD_REQUEST));
		}
		
		// Resize
		
		logger.info(String.format("Resizing URL (%s) -> %d x %d", source, width, height)); 
		OutputStream outputStream = response.getOutputStream();
		InputStream inputStream = new URL(source).openStream();
		return doResize(outputStream, inputStream, width, height);
	}
	
	private CompletableFuture<ResponseEntity<?>> doResize(OutputStream outputStream, InputStream inputStream, int targetWidth, int targetHeight) {
		return imageService
					.resize(outputStream, DEFAULT_OUTPUT_FORMAT, inputStream, targetWidth, targetHeight)
					.handle((result, ex) -> {
						if (ex != null) {
							// ERROR
							
							logger.error("Could not resize image.", ex);
							return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR);
						} else {
							// Image resized
							
							return buildResponse(HttpStatus.OK);
						}
					});
	}
	
	/** Crop endpoints */
	
	@Async("imagePoolTaskExecutor")
	@GetMapping(value = "/crop/{x},{y},{width},{height}/{filename:.+}")
	public CompletableFuture<ResponseEntity<?>>
				cropFile(@PathVariable("x") int x,
						 @PathVariable("y") int y,
						 @PathVariable("width") int width,
						 @PathVariable("height") int height,
						 @PathVariable("filename") String filename,
						 HttpServletResponse response) 
					throws IOException, InterruptedException, ExecutionException {
		// Check parameters
		
		if (x < 0 || y < 0) {
			// Bad starting point
			
			logger.warn("Bad crop offset specified.");
			return CompletableFuture.completedFuture(buildResponse(HttpStatus.BAD_REQUEST));
		} else if (width < 0 || height < 0) {
			// Bad target size
			
			logger.warn("Bad crop dimensions specified.");
			return CompletableFuture.completedFuture(buildResponse(HttpStatus.BAD_REQUEST));
		}
		
		// Check filename
		
		FileInfo fileInfo = fileService.findFile(filename).get();
		if (fileInfo == null) {
			// Not found...
			
			logger.warn("Cannot crop image. Unknown filename.");
			return CompletableFuture.completedFuture(buildResponse(HttpStatus.BAD_REQUEST));
		}
		
		// Crop
		
		logger.info(String.format("Crop filename: %s, position: (%d, %d), width: %d, height: %d.", filename, x, y, width, height)); 
		OutputStream outputStream = response.getOutputStream();
		InputStream inputStream = fileService.openFile(fileInfo).get();
		return doCrop(outputStream, inputStream, x, y, width, height);
	}
	
	@Async("imagePoolTaskExecutor")
	@GetMapping(value = "/crop")
	public CompletableFuture<ResponseEntity<?>>
				cropStream(@RequestParam(value = "source", required = true) String source,
					 	   @RequestParam(value = "x", required = true) int x,
						   @RequestParam(value = "y", required = true) int y,
						   @RequestParam(value = "width", required = true) int width,
						   @RequestParam(value = "height", required = true) int height,
						   HttpServletResponse response) 
					throws MalformedURLException, IOException, InterruptedException, ExecutionException {
		// Check parameters
		
		if (x < 0 || y < 0) {
			// Bad starting point
			
			logger.warn("Bad crop offset specified.");
			return CompletableFuture.completedFuture(buildResponse(HttpStatus.BAD_REQUEST));
		} else if (width < 0 || height < 0) {
			// Bad target size
			
			logger.warn("Bad crop dimensions specified.");
			return CompletableFuture.completedFuture(buildResponse(HttpStatus.BAD_REQUEST));
		} else if (!StringUtils.hasText(source)) {
			// Missing source
			
			logger.warn("Missing source for crop.");
			return CompletableFuture.completedFuture(buildResponse(HttpStatus.BAD_REQUEST));
		}
		
		// Crop
		
		logger.info(String.format("Crop source: %s, position: (%d, %d), width: %d, height: %d.", source, x, y, width, height)); 
		OutputStream outputStream = response.getOutputStream();
		InputStream inputStream = new URL(source).openStream();
		return doCrop(outputStream, inputStream, x, y, width, height);
	}
	
	private CompletableFuture<ResponseEntity<?>> doCrop(OutputStream outputStream, InputStream inputStream, int x, int y, int width, int height) {
		final BoundingBox bbox = new BoundingBox(x, y, x + width, y + height);
		return imageService
					.crop(outputStream, DEFAULT_OUTPUT_FORMAT, inputStream, bbox)
					.handle((result, ex) -> {
						if (ex != null) {
							// ERROR
							
							logger.error("Could not crop image.", ex);
							return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR);
						} else {
							// Image resized
							
							return buildResponse(HttpStatus.OK);
						}
					});
	}
	
	@Scheduled(fixedDelay = 5000, initialDelay = 10000)
	protected void imageProcessingTask() {
		// Check if there any image requests
		
		PdfImageRequest request;
		while ((request = imageProcessingMessageStore.pop()) != null) {
			LocalDateTime startTime = LocalDateTime.now();
	
			try {
				// Open source file
			
				FileInfo fileInfo = fileService.findFile(request.getFilename()).get();
				if (fileInfo != null) {
					// Get input stream
					
					InputStream inputStream = fileService.openFileStream(fileInfo);
					
					// Create output stream
					
					String outputFn = request.getMappedFilename();
					String contentType = "jpg".equals(request.getFormat().toLowerCase()) ? "image/jpeg" : "image/png";
					OutputStream outputStream = fileService.openUploadStream(outputFn, contentType);
					
					// Convert PDF to image
					
					int pageIndex = request.getPageIndex(), dpi = request.getDpi();
					imageService.pdfToImage(outputStream, request.getFormat(), inputStream, pageIndex, dpi);
					
					// WARNING: The next call is important!!! GridFS will drop the file if close() is not called.
					
					outputStream.close();
				}
			} catch (Exception ex) {
				// ERROR
				
				logger.error(String.format("Unable to process: %s.", request.getFilename()), ex);
			} finally {
			    // Log processing time message
			    
				long ms = Duration.between(startTime, LocalDateTime.now()).toMillis();
			    logger.info(String.format("Image '%s' -> '%s' processed (%s ms).", request.getFilename(), request.getMappedFilename(), Long.toString(ms)));
			}
		}
	}
}
