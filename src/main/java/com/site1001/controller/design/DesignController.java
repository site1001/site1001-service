package com.site1001.controller.design;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.site1001.config.AppConfig;
import com.site1001.controller.BaseController;
import com.site1001.domain.design.LayerElement;
import com.site1001.service.design.DesignService;

@RestController
@RequestMapping("/" + AppConfig.API_VERSION + "/design")
public class DesignController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(DesignController.class);
	
	@Autowired
	private DesignService designService;
	
	/** List */
	
	@Async
	@GetMapping(value = "/layer/list")
	public CompletableFuture<ResponseEntity<List<LayerElement>>> 
				getLayers(@RequestParam(value = "docName", required = true) String docName,
						  @RequestParam(value = "index", required = true) int index,
						  @RequestParam(value = "page", required = false, defaultValue = "0") int page,
						  @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit) {
		return designService
					.findAllLayers(docName, index, page, limit)
					.handle((result, ex) -> {
						if (ex != null) {
	    					// ERROR
							
							if (ex instanceof IllegalArgumentException) {
								// Bad argument
								
		    					logger.warn("Bad request.", ex);
			   			        return buildResponse(HttpStatus.BAD_REQUEST); 	
							} else {
								// Fatal
								
								logger.error("An error occurred.", ex);
								return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR); 
							}
						} else {
							// Return list...
							
							if (result.isEmpty()) {
								// No content
								
								logger.info(String.format("No results for document: %s.", docName));
								return buildResponse(HttpStatus.NO_CONTENT);
							} else {
								// List of files
								
								logger.info(String.format("Found %d layers for: %s.", result.size(), docName));
								return ResponseEntity.ok(result);
							}
						}
					});
	}
	
	/** Get */
	
	@Async
	@GetMapping(value = "/layer/{id}")
	public CompletableFuture<ResponseEntity<LayerElement>> getLayer(@PathVariable("id") long id) {
		return designService
					.get(id)
					.handle((result, ex) -> {
						if (ex != null) {
	    					// ERROR
							
							if (ex instanceof IllegalArgumentException) {
								// Bad argument
								
		    					logger.warn("Bad request.", ex);
			   			        return buildResponse(HttpStatus.BAD_REQUEST); 	
							} else {
								// Fatal
								
								logger.error("An error occurred.", ex);
								return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR); 
							}
						} else {
							// Process result...
							
							if (result == null) {
								// Not found
								
								logger.info(String.format("No results for document: %d.", id));
								return buildResponse(HttpStatus.NOT_FOUND);
							} else {
								// Return layer
								
								logger.info(String.format("Found layer: %d.", id));
								return ResponseEntity.ok(result);
							}
						}
					});
	}
	
    /** Update */
    
	@Async
    @PutMapping(value = "/layer")
    public CompletableFuture<ResponseEntity<Void>> update(@Validated @RequestBody LayerElement layerElement) {
    	return designService
					.update(layerElement)
					.handle((result, ex) -> {
						if (ex != null) {
							// ERROR
							
							if (ex instanceof NoSuchElementException) {
								// Not found
								
								logger.warn("Not found.");
								return buildResponse(HttpStatus.NOT_FOUND);
							} else if (ex instanceof IllegalArgumentException || ex instanceof IllegalStateException) {
								// Bad request
								
								logger.warn("Bad request.");
								return buildResponse(HttpStatus.BAD_REQUEST);
							} else {
								// Fatal
							
								logger.error("An error occurred.", ex);
								return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR);
							}
						} else {
							// Success
							
				    	    logger.info(String.format("Updated Layer: %s.", result.getName()));
				    	    return buildResponse(HttpStatus.OK);
						}
					});
    }
	
    /** Create layer */
    
	@Async
    @PostMapping(value = "/layer")
    public CompletableFuture<ResponseEntity<Void>> create(@Validated @RequestBody LayerElement layerElement, UriComponentsBuilder ucBuilder) { 
    	return designService
	    			.save(layerElement)
					.handle((result, ex) -> {
						if (ex != null) {
							// ERROR
							
							if (ex instanceof DuplicateKeyException) {
								// Already exists
								
								logger.warn("Already exists.");
								return buildResponse(HttpStatus.CONFLICT);
							} else if (ex instanceof IllegalArgumentException || ex instanceof IllegalStateException) {
								// Bad parameter
								
								logger.warn("Bad request.");
								return buildResponse(HttpStatus.BAD_REQUEST);
							} else {
								// Fatal
								
	    						logger.error("An error occurred.", ex);
	    						return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR);
							}
						} else {
							// Success
							
				    	    logger.info(String.format("Added layer with name: %s.", layerElement.getName()));
				    		HttpHeaders headers = new HttpHeaders();
				    		headers.setLocation(ucBuilder.path("/" + AppConfig.API_VERSION + "/design/layer/{id}").buildAndExpand(layerElement.getId()).toUri());
				    		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
						}
					});
    }
	
    /** Delete */
    
	@Async
    @DeleteMapping(value = "/layer/{id}")
    public CompletableFuture<ResponseEntity<Void>> delete(@PathVariable("id") long id) {
    	return designService
					.delete(id)
					.handle((result, ex) -> {
						if (ex != null) {
							// ERROR
							
							if (ex instanceof NoSuchElementException) {
								// Not found
								
								logger.warn("Not found.");
								return buildResponse(HttpStatus.NOT_FOUND);
							} else if (ex instanceof IllegalArgumentException || ex instanceof IllegalStateException) {
								// Bad request
								
								logger.warn("Bad request.");
								return buildResponse(HttpStatus.BAD_REQUEST);
							} else {
								// Fatal
							
								logger.error("An error occurred.", ex);
								return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR);
							}
						} else {
							// Success
							
				    	    logger.info(String.format("Deleted layer, id: %d.", id));
				    	    return buildResponse(HttpStatus.OK);
						}
					});
    }
}
