package com.site1001.service.file;

import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSFindIterable;
import com.mongodb.client.gridfs.model.GridFSFile;
import com.mongodb.client.gridfs.model.GridFSUploadOptions;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.and;

@Service("fileService")
public class FileServiceImpl implements FileService {
	// FileInfo helper class
	
	private final class GridFsFileInfoImpl implements FileInfo {
		private GridFSFile gridFsFile;
		
		public GridFsFileInfoImpl(GridFSFile gridFsFile) {
			Assert.notNull(gridFsFile, "Missing bucket file!");
			this.gridFsFile = gridFsFile;
		}
		
		@Override
		public Object getId() {
			return gridFsFile.getId();
		}

		@Override
		public String getFilename() {
			return gridFsFile.getFilename();
		}
		
		@Override
		public String getContentType() {
			return gridFsFile.getMetadata().getString("contentType");
		}
		
		@Override
		public Date getModifiedDate() {
			return gridFsFile.getUploadDate();
		}

		@Override
		public long getLength() {
			return gridFsFile.getLength();
		}
		
		@Override
		public Map<String, String> getMetaData() {
			Map<String, String> metadata = new HashMap<String, String>();
			Document document = gridFsFile.getMetadata();
			if (document != null) {
				for (String key : document.keySet()) {
					if ("contentType".equals(key)) {
						continue; // Skip it...
					}
					metadata.put(key, document.getString(key));
				}
			}
			return metadata;
		}
		
		@Override
		public String getMd5() {
			return gridFsFile.getMD5();
		}
	}
	
	// Constants
	
	private static final int DATA_CHUNK_SIZE = 65536; // 64K
	private static final int MAX_PAGE_LIMIT = 16384;
	
	// Properties
	
	@Autowired
	private GridFSBucket gridFsBucket;
	
	@Autowired
	private MongoDatabase mongoDb;
	
	@Autowired
	@Qualifier("httpDateFormat")
	private SimpleDateFormat dateFormat;
			
	@Override
	public CompletableFuture<Void> downloadFile(String filename, HttpServletResponse response) {
		Assert.notNull(response, "Missing response!");
		
		// Find file
		
		GridFSFile gridFsFile = StringUtils.hasText(filename) ? gridFsBucket.find(eq("filename", filename)).first() : null;
		FileInfo fileInfo = gridFsFile != null ? new GridFsFileInfoImpl(gridFsFile) : null;
			
		// Check result
			
		if (fileInfo == null) {
			// ERROR: Not found
			
			return buildException(new NoSuchElementException("Invalid filename."));
		} 
	    
	    // Download data
		
	    try {
		    // Set content & header info
			
		    response.setContentType(fileInfo.getContentType());
		    response.setContentLengthLong(fileInfo.getLength());
		    response.setHeader("Cache-Control", "public, max-age=3600");
		    response.setHeader("Last-Modified", dateFormat.format(fileInfo.getModifiedDate()));
		    
		    // Copy from store to output stream
		    
		    OutputStream outputStream = response.getOutputStream();
			gridFsBucket.downloadToStream((BsonValue)fileInfo.getId(), outputStream);
		    response.flushBuffer();
		    
		    // Success
		    
			return CompletableFuture.completedFuture(null);
	    } catch (Exception ex) {
			// ERROR
			
			return buildException(ex);
	    }
	}
	
	@Override
	public InputStream openFileStream(FileInfo fileInfo) {
		// Check parameters
		
		if (fileInfo == null) {
			throw new IllegalArgumentException("Missing file info.");
		}
		
		// Open file stream
		
		InputStream inputStream = gridFsBucket.openDownloadStream((BsonValue)fileInfo.getId());
		return inputStream;
	}
	
	@Override
	public CompletableFuture<InputStream> openFile(FileInfo fileInfo) {
		if (fileInfo == null) {
			// Missing file info
			
			CompletableFuture<InputStream> failedFuture = new CompletableFuture<>();
			failedFuture.completeExceptionally(new IllegalArgumentException("Invalid fileInfo."));
			return failedFuture;
		} else {
			// Return input stream
			
			InputStream inputStream = gridFsBucket.openDownloadStream((BsonValue)fileInfo.getId());
			return CompletableFuture.completedFuture(inputStream);
		}
	}
	
	@Override
	public InputStream openFile(BsonValue id) {
		// Check id
		
		if (id == null) {
			throw new IllegalArgumentException("Invalid id.");
		}
		
		// Open data stream
		
		InputStream inputStream = gridFsBucket.openDownloadStream(id);
		return inputStream;
	}
	
	protected GridFSUploadOptions buildUploadOptions(String contentType) throws Exception {
		// Check parameters
		
		if (!StringUtils.hasText(contentType)) {
			throw new IllegalArgumentException("Bad content type.");
		}
		
		// Create options
		
		Document metadata = new Document("contentType", contentType);
		GridFSUploadOptions uploadOptions = new GridFSUploadOptions().chunkSizeBytes(DATA_CHUNK_SIZE).metadata(metadata);
		return uploadOptions;
	}
	
	protected boolean exists(final String filename) {
		GridFSFindIterable iter = gridFsBucket.find(eq("filename", filename));
        return iter.first() != null;
	}
	
	@Override
	public Object uploadFromStream(InputStream inputStream, String filename, String contentType) throws Exception {
		Assert.notNull(gridFsBucket, "GridFSBucket cannot be null.");
		
		// Check parameters
		
		if (inputStream == null) {
			throw new IllegalArgumentException("Missing input stream.");
		} else if (!StringUtils.hasText(filename)) {
			throw new IllegalArgumentException("Missing filename.");
		}
		
		// Check for conflict
		
        if (exists(filename)) {
        	throw new IllegalStateException("Filename conflict.");
        }
		
		// Create metadata & upload
		
		GridFSUploadOptions uploadOptions = buildUploadOptions(contentType);
		Object fileId = gridFsBucket.uploadFromStream(filename, inputStream, uploadOptions);
		return fileId;
	}
	
	@Override
	public OutputStream openUploadStream(String filename, String contentType) throws Exception {
		Assert.notNull(gridFsBucket, "GridFSBucket cannot be null.");
		
		// Check parameters
		
		if (!StringUtils.hasText(filename)) {
			throw new IllegalArgumentException("Missing filename.");
		} 
		
		// Check for conflict
		
        if (exists(filename)) {
        	throw new IllegalStateException("Filename conflict.");
        }
		
		// Create metadata & upload
		
		GridFSUploadOptions uploadOptions = buildUploadOptions(contentType);		
		OutputStream outputStream = gridFsBucket.openUploadStream(filename, uploadOptions);
		return outputStream;
	}
	
	@Override
	public CompletableFuture<List<String>> uploadFiles(HttpServletRequest request, boolean allowMultipleFiles) {
		Assert.notNull(request, "Missing request object.");
		try {
			// Create list for copied names
			
			List<String> results = new ArrayList<String>();
			
			// Create file upload object
	       
			ServletFileUpload fileUpload = new ServletFileUpload();
			FileItemIterator fileItemIter = fileUpload.getItemIterator(request);
			while (fileItemIter.hasNext()) {
				// Process item
					
				FileItemStream fileItemStream = fileItemIter.next();
			    if (!fileItemStream.isFormField()
			    	&& "file".equals(fileItemStream.getFieldName())
			        && StringUtils.hasText(fileItemStream.getName())
			        && StringUtils.hasText(fileItemStream.getContentType())) {
			        try {
			        	// Store file
			        	
				        String filename = fileItemStream.getName(), contentType = fileItemStream.getContentType();
				        InputStream inputStream = fileItemStream.openStream();
				        uploadFromStream(inputStream, filename, contentType);
						
				        // Update state
				        
				        results.add(filename);
				        if (!allowMultipleFiles) {
				        	break;
				        }
			        } catch (Exception iex) {
			        	// ERROR: Missing parameter
			        	
			        	if (!allowMultipleFiles) {				        	
							CompletableFuture<List<String>> failedFuture = new CompletableFuture<>();
							failedFuture.completeExceptionally(iex);
							return failedFuture;
			        	}
			        }
			    }
			}
			
			// Return list of copied filenames
			
			return CompletableFuture.completedFuture(results);
		} catch (Exception ex) {
			// ERROR
			
			CompletableFuture<List<String>> failedFuture = new CompletableFuture<>();
			failedFuture.completeExceptionally(ex);
			return failedFuture;
		}
	}
	
	@Override
	public CompletableFuture<List<String>> replaceFiles(HttpServletRequest request, boolean allowMultipleFiles) {
		Assert.notNull(request, "Missing request object.");
		try {
			// Create list for copied names
			
			List<String> results = new ArrayList<String>();
			
			// Create file upload object
	       
			ServletFileUpload fileUpload = new ServletFileUpload();
			FileItemIterator fileItemIter = fileUpload.getItemIterator(request);
			while (fileItemIter.hasNext()) {
				// Process item
					
				FileItemStream fileItemStream = fileItemIter.next();
			    if (!fileItemStream.isFormField()
			    	&& "file".equals(fileItemStream.getFieldName())
			        && StringUtils.hasText(fileItemStream.getName())
			        && StringUtils.hasText(fileItemStream.getContentType())) {
			        try {
			        	// Store file (with temporary filename)
			        	
			        	String tmpFn = UUID.randomUUID().toString(), contentType = fileItemStream.getContentType();
				        InputStream inputStream = fileItemStream.openStream();
				        ObjectId tmpFileId = (ObjectId)uploadFromStream(inputStream, tmpFn, contentType);
						
				        // Get content type
				      
				        Map<String, String> metadata = new HashMap<String, String>();
						GridFSFile tmpFsFile = gridFsBucket.find(eq("_id", tmpFileId)).first();
						String tmpContentType = null;
						if (tmpFsFile != null) {
							tmpContentType = tmpFsFile.getMetadata().getString("contentType");
						}
						
						// Get metadata & delete existing file
						
				        String dstFn = fileItemStream.getName();
						GridFSFile gridFsFile = gridFsBucket.find(eq("filename", dstFn)).first();
						if (gridFsFile != null) {							
							metadata.putAll(new GridFsFileInfoImpl(gridFsFile).getMetaData());
							gridFsBucket.delete(gridFsFile.getId());
						}
						
						// Rename temporary file
						
						gridFsBucket.rename(tmpFileId, dstFn);
						
						// Update metadata
						
						if (!metadata.isEmpty()) {
							if (tmpContentType != null)	{
								metadata.put("contentType", tmpContentType);
							}
							MongoCollection<Document> mongoCollection = mongoDb.getCollection("fs.files");
							BasicDBObject tmp = new BasicDBObject("metadata", metadata);
							mongoCollection.updateOne(eq("_id", tmpFileId), new Document("$set", tmp));
						}
						
				        // Update state
				        
				        results.add(dstFn);
				        if (!allowMultipleFiles) {
				        	break;
				        }
			        } catch (Exception iex) {
			        	// ERROR: Missing parameter
			        	
			        	if (!allowMultipleFiles) {				        	
							CompletableFuture<List<String>> failedFuture = new CompletableFuture<>();
							failedFuture.completeExceptionally(iex);
							return failedFuture;
			        	}
			        }
			    }
			}
			
			// Return list of copied filenames
			
			return CompletableFuture.completedFuture(results);
		} catch (Exception ex) {
			// ERROR
			
			CompletableFuture<List<String>> failedFuture = new CompletableFuture<>();
			failedFuture.completeExceptionally(ex);
			return failedFuture;
		}
	}
	
	@Override
	public CompletableFuture<FileInfo> findFile(String filename) {
		if (!StringUtils.hasText(filename)) {
			// Invalid filename
			
			CompletableFuture<FileInfo> failedFuture = new CompletableFuture<>();
			failedFuture.completeExceptionally(new IllegalArgumentException("Invalid filename."));
			return failedFuture;
		} else {
			// Search for file
			
			GridFSFile gridFsFile = gridFsBucket.find(eq("filename", filename)).first();
			FileInfo fileInfo = gridFsFile != null ? new GridFsFileInfoImpl(gridFsFile) : null;
			return CompletableFuture.completedFuture(fileInfo);
		}
	}

	@Override
	public CompletableFuture<Void> updateMetaData(String filename, Map<String, String> metadata) {
		// Check arguments
		
		if (!StringUtils.hasText(filename)) {
			// ERROR: Invalid filename
			
			return buildException(new IllegalArgumentException("Missing filename."));
		} else if (metadata == null) {
			// ERROR: Missing metadata
			
			return buildException(new IllegalArgumentException("Metadata cannot be null."));
		}
		
		try {
			// Get file info
			
			GridFSFile gridFsFile = gridFsBucket.find(eq("filename", filename)).first();
			if (gridFsFile == null) {
				// ERROR: Not found
			
				return buildException(new NoSuchElementException("File not found."));
			}
			
			// Combine metadata
			
			GridFsFileInfoImpl fileInfo = new GridFsFileInfoImpl(gridFsFile);
			Map<String, String> combined = fileInfo.getMetaData();
			combined.put("contentType", fileInfo.getContentType());
			combined.putAll(metadata);
			
			// Save result
			
			MongoCollection<Document> mongoCollection = mongoDb.getCollection("fs.files");
			BasicDBObject tmp = new BasicDBObject("metadata", combined);
			mongoCollection.updateOne(eq("_id", gridFsFile.getId()), new Document("$set", tmp));
			
			// Success
			
			return CompletableFuture.completedFuture(null);
		} catch (Exception ex) {
			// ERROR
			
			return buildException(ex);
		}
	}
	
	@Override
	public CompletableFuture<List<FileInfo>> findAll(String contentType, int page, int limit) {
		// Check parameters
		
		if (page < 0 || limit < 1 || limit > MAX_PAGE_LIMIT) {
			// Missing file info
			
			CompletableFuture<List<FileInfo>> failedFuture = new CompletableFuture<>();
			failedFuture.completeExceptionally(new IllegalArgumentException("Invalid paging data."));
			return failedFuture;
		} 
		
		// Create list
		
		List<FileInfo> files = new ArrayList<FileInfo>();
		
		// Get GridFS iterator
		
		GridFSFindIterable findIter =
			StringUtils.hasText(contentType)
			? gridFsBucket.find(eq("metadata.contentType", contentType))
			: gridFsBucket.find();
		if (findIter != null) {
			// Set the offset & batch size (perf. option)
			
			int offset = page * limit;
			findIter.skip(offset);
			findIter.batchSize(Math.min(MAX_PAGE_LIMIT, limit));
			
			// Add files
			
			MongoCursor<GridFSFile> cursor = findIter.iterator();
			while (cursor.hasNext() && files.size() < limit) {
				GridFSFile fsFile = cursor.next();
				files.add(new GridFsFileInfoImpl(fsFile));
			}
			cursor.close();
		}
		
		// Return files
		
		return CompletableFuture.completedFuture(files);
	}
	
	@Override
	public CompletableFuture<List<FileInfo>> findByMetadata(Map<String, String> metadata, int page, int limit) {
		// Check parameters
		
		if (page < 0 || limit < 1 || limit > MAX_PAGE_LIMIT) {
			// Missing file info
			
			CompletableFuture<List<FileInfo>> failedFuture = new CompletableFuture<>();
			failedFuture.completeExceptionally(new IllegalArgumentException("Invalid paging data."));
			return failedFuture;
		} else if (metadata == null || metadata.isEmpty()) {
			// Missing search data
			
			CompletableFuture<List<FileInfo>> failedFuture = new CompletableFuture<>();
			failedFuture.completeExceptionally(new IllegalArgumentException("Missing search data."));
			return failedFuture;
		} 
		
		// Create list of BSON filters
		
		List<Bson> filterList = new ArrayList<Bson>();
		for (String key : metadata.keySet()) {
			filterList.add(eq(String.format("metadata.%s",  key), metadata.get(key)));
		}
	
		// Create list
		
		List<FileInfo> files = new ArrayList<FileInfo>();
		
		// Get GridFS iterator
	
		GridFSFindIterable findIter = gridFsBucket.find(and(filterList.toArray(new Bson[0])));
		if (findIter != null) {
			// Set the offset & batch size (perf. option)
			
			int offset = page * limit;
			findIter.skip(offset);
			findIter.batchSize(Math.min(MAX_PAGE_LIMIT, limit));
			
			// Add files
			
			MongoCursor<GridFSFile> cursor = findIter.iterator();
			while (cursor.hasNext() && files.size() < limit) {
				GridFSFile fsFile = cursor.next();
				files.add(new GridFsFileInfoImpl(fsFile));
			}
			cursor.close();
		}
		
		// Return files
		
		return CompletableFuture.completedFuture(files);
	}
	
	@Override
	public CompletableFuture<Void> rename(String oldFilename, String newFilename) {
		// Check new filename
		
		if (!StringUtils.hasText(newFilename)) {
			// ERROR: Invalid filename
			
			return buildException(new IllegalArgumentException("Invalid filename."));
		}
		
		// Find file
		
		GridFSFile gridFsFile = StringUtils.hasText(oldFilename) ? gridFsBucket.find(eq("filename", oldFilename)).first() : null;
		FileInfo fileInfo = gridFsFile != null ? new GridFsFileInfoImpl(gridFsFile) : null;
			
		// Check result
			
		if (fileInfo == null) {
			// ERROR: Not found
			
			return buildException(new NoSuchElementException("Not found."));
		} else {
			// Rename
			
			gridFsBucket.rename((BsonValue)fileInfo.getId(), newFilename);
			return CompletableFuture.completedFuture(null);
		}
	}
	
	@Override
	public CompletableFuture<Void> delete(String filename) {
		// Find file
			
		GridFSFile gridFsFile = StringUtils.hasText(filename) ? gridFsBucket.find(eq("filename", filename)).first() : null;
		FileInfo fileInfo = gridFsFile != null ? new GridFsFileInfoImpl(gridFsFile) : null;
			
		// Check result
			
		if (fileInfo == null) {
			// ERROR: Not found
			
			return buildException(new NoSuchElementException("Not found."));
		} else {
			// Delete file
				
			gridFsBucket.delete((BsonValue)fileInfo.getId());
			return CompletableFuture.completedFuture(null);
		}
	}
	
	private CompletableFuture<Void> buildException(Exception ex) {
		// Return exception future object
		
		Assert.notNull(ex, "Missing exception.");
		CompletableFuture<Void> failedFuture = new CompletableFuture<>();
		failedFuture.completeExceptionally(ex);
		return failedFuture;
	}
}
