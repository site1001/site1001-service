package com.site1001.service.file;

import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

public interface FileInfo {
	@JsonIgnore
	public Object getId();
	
	public String getFilename();
	
	public String getContentType();
	
	public long getLength();
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "UTC")
	public Date getModifiedDate();
	
	public Map<String, String> getMetaData();
	
	public String getMd5();
}