package com.site1001.service.file;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bson.BsonValue;

public interface FileService {
	/** Download */
	
	public CompletableFuture<Void> downloadFile(String filename, HttpServletResponse response);
	
	/** Read */
	
	public InputStream openFileStream(FileInfo fileInfo);
	public CompletableFuture<InputStream> openFile(FileInfo fileInfo);
	public InputStream openFile(BsonValue id);
	
	/** Upload */
	
	public Object uploadFromStream(InputStream inputStream, String filename, String contentType) throws Exception;
	public OutputStream openUploadStream(String filename, String contentType) throws Exception;
	public CompletableFuture<List<String>> uploadFiles(HttpServletRequest request, boolean allowMultipleFiles);
	
	/** Replace */
	
	public CompletableFuture<List<String>> replaceFiles(HttpServletRequest request, boolean allowMultipleFiles);
	
	/** Search */
	
	public CompletableFuture<FileInfo> findFile(String filename);
	public CompletableFuture<List<FileInfo>> findAll(String contentType, int page, int limit);
	public CompletableFuture<List<FileInfo>> findByMetadata(Map<String, String> metadata, int page, int limit);
	
	/** Rename */
	
	public CompletableFuture<Void> rename(String oldFilename, String newFilename);
	
	/** Delete */
	
	public CompletableFuture<Void> delete(String filename);
	
	/** Updates */
	
	public CompletableFuture<Void> updateMetaData(String filename, Map<String, String> metadata);
}
