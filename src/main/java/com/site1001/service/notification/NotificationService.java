package com.site1001.service.notification;

import java.util.concurrent.CompletableFuture;

import com.site1001.domain.notification.Subscription;

public interface NotificationService {
	/** Count */
	
    public CompletableFuture<Long> count();
	
    /** Get one */
    
    public CompletableFuture<Subscription> get(long id);

    /** Save */
    
    public CompletableFuture<Void> save(Subscription subscription);
    
    /** Broadcast */
    
    public CompletableFuture<Void> broadcast(final String tag, final String title, final String message);
}
