package com.site1001.service.notification;

import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.google.gson.JsonObject;
import com.site1001.domain.notification.Subscription;
import com.site1001.repo.notification.SubscriptionRepository;

import nl.martijndwars.webpush.Notification;
import nl.martijndwars.webpush.PushService;

@Service("notificationService")
@Transactional
public class NotificationServiceImpl implements NotificationService {
	@Autowired
	private SubscriptionRepository subscriptionRepo;
	
	@Autowired
	private PushService pushService;
	
	@Override
    public CompletableFuture<Long> count() {
		return CompletableFuture.completedFuture(subscriptionRepo.count());
    }
	
	@Override
    public CompletableFuture<Subscription> get(long id) {
		return CompletableFuture.completedFuture(subscriptionRepo.findOne(id));
    }

	@Override
    public CompletableFuture<Void> save(Subscription subscription) {
		if (subscription == null) {
			// ERROR: Bad argument
			
			return buildException(new IllegalArgumentException("Bad subscription object."));
		} else if (!StringUtils.hasText(subscription.getTag())) {
			// ERROR: Missing tag
			
			return buildException(new IllegalStateException("Missing tag."));
		} else if (!StringUtils.hasText(subscription.getEndpoint())) {
			// ERROR: Missing end point
			
			return buildException(new IllegalStateException("Missing endpoint."));
		} else if (!subscriptionRepo.findByPushKey(subscription.getPushKey()).isEmpty()) {
			// ERROR: Duplicate entry
			
			return buildException(new DuplicateKeyException("Auth already exists."));
		} else if (subscription.getId() != null) {
			// ERROR: Bad state
			
			return buildException(new IllegalStateException("Bad key."));
		} else {
			// Save...
			
			subscriptionRepo.saveAndFlush(subscription);
			return CompletableFuture.completedFuture(null);
		}
    }
	
    private byte[] getPayload(final String title, final String message) {
    	Assert.hasText(title, "Missing title.");
    	Assert.hasText(message, "Missing message.");
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("title", title);
        jsonObject.addProperty("message", message);
        return jsonObject.toString().getBytes();
    }
	
    public CompletableFuture<Void> broadcast(final String tag, final String title, final String message) {
    	// Check parameters
    	
    	if (!StringUtils.hasText(tag)) {
    		// Missing tag
    		
    		return buildException(new IllegalArgumentException("Missing tag."));
    	} else if (!StringUtils.hasText(title)) {
    		// Bad argument
    		
    		return buildException(new IllegalArgumentException("Missing title."));
    	} else if (!StringUtils.hasText(message)) {
    		// Bad argument
    		
    		return buildException(new IllegalArgumentException("Missing message."));
    	}
    	
    	// Create payload
    	
    	byte[] payload = getPayload(title, message);
    	
    	// Push to all subscriptions
    	
    	int page = 0, limit = 100;
    	Page<Subscription> subs = subscriptionRepo.findByTag(tag, new PageRequest(page, limit));
    	for (; !subs.getContent().isEmpty(); ++page, subs = subscriptionRepo.findByTag(tag, new PageRequest(page, limit))) {
            for (Subscription sub : subs.getContent()) {
				try {
					Notification notification = new Notification(sub.getEndpoint(), sub.getPushKey(), sub.getAuth(), payload);
	           		pushService.sendAsync(notification);
				} catch (Exception ex) {
				}
            }
    	}
    	
    	// Return 
    	
    	return CompletableFuture.completedFuture(null);
    }
    
	private <T> CompletableFuture<T> buildException(Exception ex) {
		CompletableFuture<T> failedFuture = new CompletableFuture<>();
		failedFuture.completeExceptionally(ex);
		return failedFuture;
	}
}
