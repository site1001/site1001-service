package com.site1001.service.image;

import java.util.UUID;

import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.integration.mongodb.store.MongoDbMessageStore;

public class ImageProcessingMessageStore extends MongoDbMessageStore {
	private static final String COLLECTION_NAME = "imageProcCollection";
	private final UUID imageProfGroupId;
	
	public ImageProcessingMessageStore(UUID groupId, MongoDbFactory dbFactory) {
		super(dbFactory, COLLECTION_NAME);
		
		// Set the group id
		
		if (groupId == null) {
			throw new NullPointerException("GroupId cannot be null.");
		}
		this.imageProfGroupId = groupId;
	}
	
	protected Object getGroupId() {
		return imageProfGroupId;
	}
	
	public void push(PdfImageRequest request) {
		Message<PdfImageRequest> msg = MessageBuilder.withPayload(request).build(); 
		addMessageToGroup(getGroupId(), msg);
	}
	
	public PdfImageRequest pop() {
		Message<?> msg = pollMessageFromGroup(getGroupId());
		return msg != null ? (PdfImageRequest)msg.getPayload() : null;
	}
}
