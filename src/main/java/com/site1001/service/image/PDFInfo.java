package com.site1001.service.image;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class PDFInfo implements Serializable {
	private static final long serialVersionUID = 1567527257292997085L;
	private int numberOfPages;
	private double rotation;
	private String docVersion;
	private List<Map<String, String>> pageInfo;
	private Map<String, String> docInfo;
	private String pageLayout;
	private String pageMode;
	private String language;
	private float pdfVersion;
	private boolean encrypted;
	private String pageSize;
	private float pageWidth;
	private float pageHeight;
	private String keywords;
	
	public PDFInfo() {
	}
	
	public void setPageInfo(List<Map<String, String>> pageInfo) {
		this.pageInfo = pageInfo;
	}
	
	public List<Map<String, String>> getPageInfo() {
		return pageInfo;
	}
	
	public void setNumberOfPages(int numberOfPages) {
		this.numberOfPages = numberOfPages;
	}
	
	public int getNumberOfPages() {
		return numberOfPages;
	}
	
	public void setRotation(double rotation) {
		this.rotation = rotation;
	}
	
	public double getRotation() {
		return rotation;
	}
	
	public void setDocVersion(String docVersion) {
		this.docVersion = docVersion;
	}
	
	public String getDocVersion() {
		return docVersion;
	}
	
	public void setDocInfo(Map<String, String> metadata) {
		this.docInfo = metadata;
	}
	
	public Map<String, String> getDocInfo() {
		return docInfo;
	}
	
	public void setPageLayout(String pageLayout) {
		this.pageLayout = pageLayout;
	}
	
	public String getPageLayout() {
		return pageLayout;
	}
	
	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}
	
	public String getPageMode() {
		return pageMode;
	}
	
	public void setLanguage(String language) {
		this.language = language;
	}
	
	public String getLanguage() {
		return language;
	}
	
	public void setPdfVersion(float pdfVersion) {
		this.pdfVersion = pdfVersion;
	}
	
	public float getPdfVersion() {
		return pdfVersion;
	}
	
	public void setEncrypted(boolean encrypted) {
		this.encrypted = encrypted;
	}
	
	public boolean getEncrypted() {
		return encrypted;
	}	
	
	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}
	
	public String getPageSize() {
		return pageSize;
	}
	
	public void setPageWidth(float pageWidth) {
		this.pageWidth = pageWidth;
	}
	
	public float getPageWidth() {
		return pageWidth;
	}
	
	public void setPageHeight(float pageHeight) {
		this.pageHeight = pageHeight;
	}
	
	public float getPageHeight() {
		return pageHeight;
	}
	
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	
	public String getKeywords() {
		return keywords;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Integer.hashCode(numberOfPages);
		result = prime * result + Boolean.hashCode(encrypted);
		result = prime * result + (docVersion != null ? docVersion.hashCode() : 0);
		result = prime * result + (docInfo != null ? docInfo.hashCode() : 0);
		result = prime * result + Float.hashCode(pdfVersion);
		result = prime * result + (pageInfo != null ? pageInfo.hashCode() : 0);
		result = prime * result + (pageLayout != null ? pageLayout.hashCode() : 0);
		result = prime * result + (pageMode != null ? pageMode.hashCode() : 0);
		result = prime * result + (language != null ? language.hashCode() : 0);
		result = prime * result + (pageSize != null ? pageSize.hashCode() : 0);
		result = prime * result + Float.hashCode(pageWidth);
		result = prime * result + Float.hashCode(pageHeight);
		result = prime * result + (keywords != null ? keywords.hashCode() : 0);
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null) {
			return false;
		} else if (getClass() != obj.getClass()) {
			return false;
		}
		PDFInfo other = (PDFInfo)obj;
		return numberOfPages == other.numberOfPages
			   && pdfVersion == other.pdfVersion
			   && encrypted == other.encrypted
			   && pageWidth == other.pageWidth
			   && pageHeight == other.pageHeight
			   && (keywords == null ? other.keywords == null : keywords.equals(other.keywords))
			   && (pageInfo == null ? other.pageInfo == null : pageInfo.equals(other.pageInfo))
			   && (pageMode == null ? other.pageMode == null : pageMode.equals(other.pageMode))
			   && (language == null ? other.language == null : language.equals(other.language))
			   && (pageLayout == null ? other.pageLayout == null : pageLayout.equals(other.pageLayout))
			   && (docInfo == null ? other.docInfo == null : docInfo.equals(other.docInfo))
			   && (docVersion == null ? other.docVersion == null : docVersion.equals(other.docVersion))
			   && (pageSize == null ? other.pageSize == null : pageSize.equals(other.pageSize));
	}
	
	@Override
    public String toString() {
        return String.format("PDFInfo [numberOfPages: %d, pageInfo: %s, version: '%s', docInfo: '%s', pageLayout: '%s', pageMode: '%s', language: '%s', pdfVersion: %f, encrypted: %s pageSize: %s, pageWidth: %f, pageHeight: %f, keywords: '%s']",
        					 numberOfPages,
        					 pageInfo.toString(),
        					 docVersion,
        					 docInfo.toString(),
        					 pageLayout,
        					 pageMode,
        					 language,
        					 pdfVersion,        					 
        					 Boolean.toString(encrypted),
        					 pageSize,
        					 pageWidth,
        					 pageHeight,
        					 keywords);
    }
}
