package com.site1001.service.image;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.imageio.ImageIO;

import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Rotation;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

@Service("imageService")
public class ImageServiceImpl implements ImageService {
	/** Got PDF info. */
	
	public CompletableFuture<PDFInfo> getPDFInfo(InputStream inputStream) {
		try {
			// Open pdf document
			
	    	PDDocument document = PDDocument.load(inputStream);
	    	PDDocumentCatalog catalog = document.getDocumentCatalog();
	    	int numberOfPages = document.getNumberOfPages();
	    	
	    	// Set general info
	    	
			PDFInfo pdfInfo = new PDFInfo();
	    	pdfInfo.setPdfVersion(document.getVersion());
	    	pdfInfo.setDocVersion(catalog.getVersion());
	    	pdfInfo.setNumberOfPages(numberOfPages);
	    	pdfInfo.setLanguage(catalog.getLanguage());
	    	pdfInfo.setPageLayout(catalog.getPageLayout().toString());
	    	pdfInfo.setPageMode(catalog.getPageMode().toString());
	    	pdfInfo.setEncrypted(document.isEncrypted());
	    	
	    	// Get page info
	    	
	    	List<Map<String, String>> pageInfo = new ArrayList<Map<String, String>>();
	    	pdfInfo.setPageInfo(pageInfo);
            float mediaBoxWidthInPoints = -1, mediaBoxHeightInPoints = -1;
	    	for (int pageIndex = 0; pageIndex < numberOfPages; ++pageIndex) {
	    		// Get page
	    		
	    		PDPage page = document.getPage(pageIndex);
	    		
	    		// Get dimensions
	    		
                PDRectangle mediaBox = page.getMediaBox();
                if (mediaBox != null) {
                    mediaBoxWidthInPoints = mediaBox.getWidth();
                    mediaBoxHeightInPoints = mediaBox.getHeight();
                }
                
                // Get attributes
                
	    		Map<String, String> infoMap = new HashMap<String, String>();
	    		pageInfo.add(infoMap);
	    		COSDictionary dictionary = page.getCOSObject();
		    	for (COSName name : dictionary.keySet()) {
		    		infoMap.put(name.getName(), dictionary.getNameAsString(name));
		    	}
	    	}
	    	
	    	// Set document information
	    	
	    	PDDocumentInformation docInfo = document.getDocumentInformation();
	    	if (docInfo != null) {
		    	COSDictionary dictionary = docInfo.getCOSObject();
		    	Map<String, String> infoMap = new HashMap<String, String>();
		    	for (COSName name : dictionary.keySet()) {
		    		infoMap.put(name.getName(), dictionary.getNameAsString(name));
		    	}
		    	pdfInfo.setDocInfo(infoMap);
		    	pdfInfo.setPageSize(String.format("%.2fx%.2f pts", mediaBoxWidthInPoints,  mediaBoxHeightInPoints));
	            pdfInfo.setPageWidth(mediaBoxWidthInPoints);
	            pdfInfo.setPageHeight(mediaBoxHeightInPoints);
	            pdfInfo.setKeywords(docInfo.getKeywords());
	    	}
	    	
	    	// Clean-up
	    	
	    	document.close();
	    	inputStream.close();
	    		
			// Return
			
			return CompletableFuture.completedFuture(pdfInfo);
		} catch (Exception ex) {
			// ERROR
			
			CompletableFuture<PDFInfo> failedFuture = new CompletableFuture<>();
			failedFuture.completeExceptionally(ex);
			return failedFuture;
		}
	}
	
	/** PDF to image */
	
	@Override
	public void pdfToImage(OutputStream outputStream, String fileFormat, InputStream inputStream, int pageIndex, int dpi) throws Exception {
		// Check parameters
		
		if (!StringUtils.hasText(fileFormat)) {
			throw new IllegalArgumentException("Missing file format.");
		} else if (inputStream == null) {	
			throw new IllegalArgumentException("Missing input stream.");
		} else if (dpi < 1 || dpi > 3600) {
			throw new IllegalArgumentException("Invalid dpi.");
		}
		
		// Open pdf document
		
    	PDDocument document = PDDocument.load(inputStream);
    	
    	// Check page index
    	
    	if (pageIndex < 0 || pageIndex >= document.getNumberOfPages()) {
    		document.close();
    		throw new IllegalArgumentException("Page index out of range.");
    	} 
	    
    	// Render page to bitmap at 2x size
    	
    	fileFormat = fileFormat.toUpperCase();
    	PDFRenderer pdfRenderer = new PDFRenderer(document);
    	dpi = 250;
    	float factor = 2.0f;
    	BufferedImage sourceImage = pdfRenderer.renderImageWithDPI(pageIndex, dpi * factor, "JPG".equals(fileFormat) ? ImageType.RGB : ImageType.ARGB);
    	
    	// Clean-up
    	
    	document.close();
    	inputStream.close();
    	
    	// Create sharpen filter
    	
    	float[] sharpen = new float[] {
		     0.0f, -1.0f, 0.0f,
		    -1.0f, 5.0f, -1.0f,
		     0.0f, -1.0f, 0.0f
    	};
    	Kernel kernel = new Kernel(3, 3, sharpen);
    	ConvolveOp op = new ConvolveOp(kernel);
    	BufferedImage filteredImage = op.filter(sourceImage, null);
    	
    	// Release source image
    	
    	int sourceWidth = sourceImage.getWidth(), sourceHeight = sourceImage.getHeight();
    	sourceImage.getGraphics().dispose();
    	sourceImage = null;
    	
		// Resize (by half)
		
		BufferedImage resizedImage =
			Scalr.resize(filteredImage, 
						 Scalr.Method.ULTRA_QUALITY, 
						 Scalr.Mode.AUTOMATIC,
						 (int)(sourceWidth / factor),
						 (int)(sourceHeight / factor));

		// Release filteredImage
		
		filteredImage.getGraphics().dispose();
		filteredImage = null;
		
		// Return output stream
		
        ImageIO.write(resizedImage, fileFormat, outputStream);
        outputStream.flush();
        
        // Release resized image
        
        resizedImage.getGraphics().dispose();
        resizedImage = null;
	}
	
	/*
	@Override
	public CompletableFuture<Void> convertPdfToImage(OutputStream outputStream, String fileFormat, InputStream inputStream, int pageIndex, int dpi) {
		// Check parameters
		
		if (outputStream == null) {
			// Invalid output stream
			
			return buildException(new IllegalArgumentException("Missing output stream."));
		} else if (!StringUtils.hasText(fileFormat)) {
			// Missing file format
			
			return buildException(new IllegalArgumentException("Missing file format."));
		} else if (inputStream == null) {	
			// Invalid input stream
			
			return buildException(new IllegalArgumentException("Missing input stream."));
		} else if (dpi < 1 || dpi > 3600) {
			// Invalid dot pitch
			
			return buildException(new IllegalArgumentException("Invalid dpi."));
		}
		
		try {
			// Open pdf document
			
	    	PDDocument document = PDDocument.load(inputStream);
	    	
	    	// Check page index
	    	
	    	if (pageIndex < 0 || pageIndex >= document.getNumberOfPages()) {
	    		document.close();
	    		return buildException(new IllegalArgumentException("Page index out of range."));
	    	} 
	    
	    	// Render page to bitmap at 2x size
	    	
	    	fileFormat = fileFormat.toUpperCase();
	    	PDFRenderer pdfRenderer = new PDFRenderer(document);
	    	dpi = 250;
	    	float factor = 2.0f;
	    	BufferedImage sourceImage = pdfRenderer.renderImageWithDPI(pageIndex, dpi * factor, "JPG".equals(fileFormat) ? ImageType.RGB : ImageType.ARGB);
	    	
	    	// Clean-up
	    	
	    	document.close();
	    	inputStream.close();
	    	
	    	// Create sharpen filter
	    	
	    	float[] sharpen = new float[] {
			     0.0f, -1.0f, 0.0f,
			    -1.0f, 5.0f, -1.0f,
			     0.0f, -1.0f, 0.0f
	    	};
	    	Kernel kernel = new Kernel(3, 3, sharpen);
	    	ConvolveOp op = new ConvolveOp(kernel);
	    	
			// Resize (by half)
			
	    	int sourceWidth = sourceImage.getWidth(), sourceHeight = sourceImage.getHeight();
			BufferedImage resizedImage =
				Scalr.resize(op.filter(sourceImage, null), 
							 Scalr.Method.ULTRA_QUALITY, 
							 Scalr.Mode.AUTOMATIC,
							 (int)(sourceWidth / factor),
							 (int)(sourceHeight / factor));
							// Scalr.OP_ANTIALIAS);
							 //Scalr.OP_BRIGHTER);
	    	
	    	// Convert image to specified format & copy to output stream
	    	
	    	Assert.hasText(fileFormat, "Invalid file format provided.");
			ImageIO.write(resizedImage, fileFormat, outputStream);
			outputStream.flush();
			
			// Return
			
			return CompletableFuture.completedFuture(null);
		} catch (Exception ex) {
			// ERROR
			
			return buildException(ex);
		}
	}
	*/
	
	/** Flip */

	@Override
	public CompletableFuture<Void> flip(OutputStream outputStream, String formatName, InputStream inputStream, int direction) {
		try {
			// Load image
			
			BufferedImage srcImage = ImageIO.read(inputStream);
			
			// Flip
			
			BufferedImage dstImage = Scalr.rotate(srcImage, direction == 1 ? Rotation.FLIP_HORZ : Rotation.FLIP_VERT, new BufferedImageOp[] {});
			ImageIO.write(dstImage, formatName, outputStream);
			outputStream.flush();
			
			// Return
			
			return CompletableFuture.completedFuture(null);
		} catch (Exception ex) {
			// ERROR
			
			return buildException(ex);
		}
	}
	
	/** Resize */
	
	@Override
	public CompletableFuture<Void> resize(OutputStream outputStream, String formatName, InputStream inputStream, int targetWidth, int targetHeight) {
		try {
			// Load image
			
			BufferedImage srcImage = ImageIO.read(inputStream);
			
			// Resize
			
			BufferedImage dstImage = Scalr.resize(srcImage, Scalr.Mode.AUTOMATIC, targetWidth, targetHeight);
			ImageIO.write(dstImage, formatName, outputStream);
			outputStream.flush();
			
			// Return
			
			return CompletableFuture.completedFuture(null);
		} catch (Exception ex) {
			// ERROR
			
			return buildException(ex);
		}
	}
	
	/** Crop */
	
	public CompletableFuture<Void> crop(OutputStream outputStream, String formatName, InputStream inputStream, BoundingBox boundingBox) {
		// Check bbox
		
		if (boundingBox == null) {
			// ERROR
			
			return buildException(new IllegalArgumentException("Missing bounding box."));
		}
		
		try {
			// Load image
			
			BufferedImage srcImage = ImageIO.read(inputStream);
			
			// Copy region
			
			BufferedImage dstImage = Scalr.crop(srcImage, boundingBox.getLeft(), boundingBox.getTop(), boundingBox.getWidth(), boundingBox.getHeight());
			ImageIO.write(dstImage, formatName, outputStream);
			outputStream.flush();
			
			// Return
			
			return CompletableFuture.completedFuture(null);
		} catch (Exception ex) {
			// ERROR
			
			return buildException(ex);
		}	
	}

	/** Exception handler */
	
	private CompletableFuture<Void> buildException(Exception ex) {
		CompletableFuture<Void> failedFuture = new CompletableFuture<>();
		failedFuture.completeExceptionally(ex);
		return failedFuture;
	}
}
