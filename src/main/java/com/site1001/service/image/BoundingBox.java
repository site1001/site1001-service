package com.site1001.service.image;

public class BoundingBox {
	private int left; 
	private int bottom; 
	private int right;
	private int top;
	
	public BoundingBox() {
		left = right = bottom = top = 0;
	}
	   
	public BoundingBox(int x1, int y1, int x2, int y2) {
	    this.left = Math.min(x1, x2);
	    this.right = Math.max(x1, x2);
	    this.top = Math.min(y1, y2);
	    this.bottom = Math.max(y1, y2);
	}
	
	public void setLowerLeft(int x, int y) {
		left = x;
		bottom = y;
	}
	
	public void setLeft(int left) {
		this.left = left;
	}
	
	public int getLeft() {
		return left;
	}
	
	public void setBottom(int bottom) {
		this.bottom = bottom;
	}
	
	public int getBottom() {
		return bottom;
	}
	
	public void setUpperRight(int x, int y) {
		right = x; 
		top = y;
	}
	
	public void setRight(int right) {
		this.right = right;
	}
	
	public int getRight() {
		return right;
	}
	
	public void setTop(int top) {
		this.top = top;
	}
	
	public int getTop() {
		return top;
	}
	
	public boolean intersects(BoundingBox other) {
	    return this.right >= other.left && this.top >= other.bottom && other.right >= this.left && other.top >= this.bottom;
	}

    public int area() {
        return (right - left) * (top - bottom);
    }
    
    public int getWidth() {
    	return right - left;
    }
    
    public int getHeight() {
    	return bottom - top;
    }
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Integer.hashCode(left);
		result = prime * result + Integer.hashCode(top);
		result = prime * result + Integer.hashCode(right);
		result = prime * result + Integer.hashCode(bottom);
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null) {
			return false;
		} else if (getClass() != obj.getClass()) {
			return false;
		}
		BoundingBox other = (BoundingBox)obj;
		return this.left == other.left
			   && this.right == other.right
			   && this.bottom == other.bottom
			   && this.top == other.top;
	}

	@Override
    public String toString() {
        return String.format("BoundingBox [left: %d, top: %d, right: %d, bottom: %d]", left, top, right, bottom);
    }
}