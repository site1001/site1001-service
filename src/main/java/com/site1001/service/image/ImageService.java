package com.site1001.service.image;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.CompletableFuture;

public interface ImageService {
	/** PDF info */
	
	public CompletableFuture<PDFInfo> getPDFInfo(InputStream inputStream);
	
	/** Convert PDF to Bitmap */
		
	public void pdfToImage(OutputStream outputStream, String fileFormat, InputStream inputStream, int pageIndex, int dpi) throws Exception;
	// public CompletableFuture<Void> convertPdfToImage(OutputStream outputStream, String fileFormat, InputStream inputStream, int pageIndex, int dpi);
	
	/** Flip */
	
	public CompletableFuture<Void> flip(OutputStream outputStream, String formatName, InputStream inputStream, int direction);
	
	/** Resize */
	
	public CompletableFuture<Void> resize(OutputStream outputStream, String formatName, InputStream inputStream, int targetWidth, int targetHeight);
	
	/** Crop */
	
	public CompletableFuture<Void> crop(OutputStream outputStream, String formatName, InputStream inputStream, BoundingBox boundingBox);
}
