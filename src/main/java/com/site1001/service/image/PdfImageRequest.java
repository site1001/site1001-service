package com.site1001.service.image;

import org.springframework.util.StringUtils;

public class PdfImageRequest {
	private final String filename;
	private final int pageIndex;
	private final int dpi;
	private final String format;
	
	public PdfImageRequest(String filename, int pageIndex, int dpi, String format) {
		// Check arguments
		
		if (!StringUtils.hasText(filename)) {
			throw new IllegalArgumentException("Image request must contain a filename.");
		} else if (filename.charAt(0) == '/') {
			throw new IllegalArgumentException("Filename cannot lead with a forward slash.");
		}
		
		this.filename = filename;
		this.pageIndex = pageIndex;
		this.dpi = dpi;
		this.format = format;
	}
	
	public String getFilename() {
		return filename;
	}
		
	public int getPageIndex() {
		return pageIndex;
	}
	
	public int getDpi() {
		return dpi;
	}
	
	public String getFormat() {
		return format;
	}
	
	public String getMappedFilename() {
		return String.format("pdf2img/processed/%s-%d-%d.%s", filename.replace('.', '-'), pageIndex, dpi, format);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (filename == null ? 0 : filename.hashCode());
		result = prime * result + Integer.hashCode(pageIndex);
		result = prime * result + Integer.hashCode(dpi);
		result = prime * result + (format == null ? 0 : format.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null) {
			return false;
		} else if (getClass() != obj.getClass()) {
			return false;
		}
		PdfImageRequest other = (PdfImageRequest)obj;
		return pageIndex == other.pageIndex
			   && dpi == other.dpi
			   && (format == null ? other.format == null : format.equals(other.format))
			   && (filename == null ? other.filename == null : filename.equals(other.filename));
	}

	@Override
    public String toString() {
        return String.format("PdfImageRequest [filename: '%s', pageIndex: %d, dpi: %d, format: '%s']",
        					 filename,
        					 pageIndex,
        					 dpi,
        					 format);
    }
}
