package com.site1001.service.design;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.site1001.domain.design.LayerElement;
import com.site1001.domain.design.Point;
import com.site1001.repo.design.LayerRepository;

@Transactional
@Service("designService")
public class DesignServiceImpl implements DesignService {
	@Autowired
	private LayerRepository layerRepo;
	
	@Override
	public CompletableFuture<LayerElement> get(long id) {
		return CompletableFuture.completedFuture(layerRepo.getOne(id));
	}
	
	@Override
	public CompletableFuture<List<LayerElement>> findAllLayers(String docName, int pageIndex, int page, int limit) {
		if (!StringUtils.hasText(docName)) {
			// Missing document name
			
			return buildException(new IllegalArgumentException("Missing document name."));
		} else if (page < 0 || limit < 1) {
			// Bad paging data
			
			return buildException(new IllegalArgumentException("Missing paging data."));
		} else {
			// Return layers
			
			PageRequest pageRequest = new PageRequest(page, limit);
			Page<LayerElement> result = layerRepo.findAllByDocNameAndPageIndex(docName, pageIndex, pageRequest);
			return CompletableFuture.completedFuture(result.getContent());
		}
	}
	
	@Modifying
	@Override
	public CompletableFuture<LayerElement> update(LayerElement layerElement) {
		if (layerElement == null) {
			// Missing item!
			
			return buildException(new IllegalArgumentException("Missing layer entity."));
		} else if (layerElement.getId() == null
				   || !StringUtils.hasText(layerElement.getName())
				   || !StringUtils.hasText(layerElement.getDocument())) {
			// EBad id, missing name or missing doc name
			
			return buildException(new IllegalStateException("Bad key."));
		} else {
			// Get existing item
			
			Long id = layerElement.getId();
			LayerElement current = layerRepo.getOne(id);
			if (current == null) {
				// Not found
				
				return buildException(new NoSuchElementException("Not found."));
			}
			
			// Update and save
			
			current.update(layerElement);
			layerRepo.saveAndFlush(current);
			return CompletableFuture.completedFuture(current);
		}
	}
	
	@Override
	public CompletableFuture<Void> save(LayerElement layerElement) {
		if (layerElement == null) {
			// Missing item!
			
			return buildException(new IllegalArgumentException("Missing layer entity."));
		} else if (layerElement.getId() != null
				   || !StringUtils.hasText(layerElement.getName())
				   || !StringUtils.hasText(layerElement.getDocument())) {
			// Bad id, missing name or missing doc name
			
			return buildException(new IllegalStateException("Bad key."));
		} else if (!layerRepo.findByNameAndDocNameAndPageIndexAndLayerType(
						layerElement.getName(), layerElement.getDocument(), layerElement.getPageIndex(),
						layerElement.getLayerType()).isEmpty()) {
			// Already exists!
		
			return buildException(new DuplicateKeyException("Layer name and type already exists."));
		} else {
			// Save...
			
			layerRepo.saveAndFlush(layerElement);
			return CompletableFuture.completedFuture(null);
		}
	}
	
	@Override
	public CompletableFuture<Void> delete(long id) {
		LayerElement current = layerRepo.getOne(id);
		if (current == null) {
			// Not found
			
			return buildException(new NoSuchElementException("Not found."));
		} else {
			// Delete
			
			layerRepo.delete(id);
			return CompletableFuture.completedFuture(null);
		}
	}
	
	@Override
	public CompletableFuture<Void> createTestData() {
		// Create geometry

		List<Point> pts = new ArrayList<>();
		//pts.add(new Point(100, 100));
		//pts.add(new Point(200, 200));
		//pts.add(new Point(150, 300));
		
		// Create layer element
		
		LayerElement layerElement = new LayerElement();
		layerElement.setDocument("test");
		layerElement.setName("test");
		layerElement.setPoints(pts);
		
		// Save it
		
		layerRepo.saveAndFlush(layerElement);
		return CompletableFuture.completedFuture(null);
	}
	
	private <T> CompletableFuture<T> buildException(Exception ex) {
		// Return exception future object
		
		Assert.notNull(ex, "Missing exception.");
		CompletableFuture<T> failedFuture = new CompletableFuture<>();
		failedFuture.completeExceptionally(ex);
		return failedFuture;
	}
}
