package com.site1001.service.design;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.site1001.domain.design.LayerElement;

public interface DesignService {
	/** Get one */
	
	public CompletableFuture<LayerElement> get(long id);
	
	/** Search */
	
	public CompletableFuture<List<LayerElement>> findAllLayers(String docName, int pageIndex, int page, int limit);
	
	/** Save */
	
	public CompletableFuture<Void> save(LayerElement layerElement);
	
	/** Update */
	
	public CompletableFuture<LayerElement> update(LayerElement layerElement);
	
	/** Delete */
	
	public CompletableFuture<Void> delete(long id);
	
	/** Test */
	
	public CompletableFuture<Void> createTestData();
}
