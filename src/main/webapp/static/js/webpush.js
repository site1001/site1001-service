'use strict';

const serviceWorkerUrl = 'https://integ-api.ore.site1001.com/static/js/service-worker.js';
const appServerPublicKey = 'BA6Q6EbNNd0JOtOVq5PhOMk5ChOFstWBeI-7xQTVLBxHAEZ1DC5MiValRpwvdPIYeo-yeduoD72aOq5hI17Zbww'; 
const serverSubscribeUrl = 'https://integ-api.ore.site1001.com/v1/notify/subscribe';

let isSubscribed = false;
let swRegistration = null;

function urlB64ToUint8Array(base64String) {
	const padding = '='.repeat((4 - base64String.length % 4) % 4);
	const base64 = (base64String + padding).replace(/\-/g, '+').replace(/_/g, '/');
	const rawData = window.atob(base64);
	const outputArray = new Uint8Array(rawData.length);
	for (let i = 0; i < rawData.length; ++i) {
		outputArray[i] = rawData.charCodeAt(i);
	}
	return outputArray;
}

function sendSubscriptionToServer(subscription) {
    // Get public key and user auth from the subscription object
	
    var key = subscription.getKey ? subscription.getKey('p256dh') : '';
    var auth = subscription.getKey ? subscription.getKey('auth') : '';
	
	// Create json
	
	var jsonBody = JSON.stringify({
		tag: 'test',
		endpoint: subscription.endpoint,
		key: key ? btoa(String.fromCharCode.apply(null, new Uint8Array(key))) : '',
		auth: auth ? btoa(String.fromCharCode.apply(null, new Uint8Array(auth))) : ''
	});

    // Send request
    
    return fetch(serverSubscribeUrl, {
    			 method: 'POST',
    			 headers: {
    				'Content-Type': 'application/json'
    			 },
    			 body: jsonBody
    		});
}

function subscribeUser() {
	// Get server key encoding
	
	const applicationServerKeyEncoding = urlB64ToUint8Array(appServerPublicKey);
	 
	// Subscribe; NOTE: application key is required for Chrome
	
	swRegistration
	.pushManager
	.subscribe({
	    userVisibleOnly: true,
	    applicationServerKey: applicationServerKeyEncoding
	})
	.then(function(subscription) {
		// Success
		
		console.log('User is subscribed');
		sendSubscriptionToServer(subscription);
	    isSubscribed = true;
	})
	.catch(function(err) {
		// Failed
		
	    console.log('Failed to subscribe the user: ', err);
	});
}

function registerServiceWorker() {
	// Check for compatibility
	
    if (!('serviceWorker' in navigator && 'PushManager' in window)) {
    	return;
    }
    
    // Register service worker
    
    return navigator
           .serviceWorker
           .register(serviceWorkerUrl)
           .then(function(registration) {
        	   console.log('Service worker successfully registered.');
        	   swRegistration = registration;
        	   subscribeUser();
           })
           .catch(function(err) {
        	   console.error('Unable to register service worker.', err);
           });
}

function askPermission() {
	return
		new Promise(function(resolve, reject) {
		    const permissionResult =
		    	Notification.requestPermission(function(result) {
		    		resolve(result);
		    	});
		    if (permissionResult) {
	            console.log('Has permission result.');
		    	permissionResult.then(resolve, reject);
		    } else {
	            console.error('Does not have permission result.');
		    }
	  	})
	  	.then(function(permissionResult) {
	  		if (permissionResult !== 'granted') {
	  			throw new Error('We weren\'t granted permission.');
	  		}
	  	})
	    .catch(function(err) {
            console.error('Unable to register service worker.', err);
        });
}

function checkServiceWorker() {
    if ('serviceWorker' in navigator && 'PushManager' in window) {
    	var registration = registerServiceWorker();
    	if (!(registration === null)) {
    		var promise = askPermission();
    		if (!(promise === null)) {
    			subscribeUserToPush();
    		}
    	}
    } else {
        console.warn('Service workers are not supported in this browser.');
    }
}
